# accounts

building command line tool:
```bash
go build -o accounts ./cmd/main.go ./cmd/command.go 
```

creating group:
```bash
./accounts group.create --group.name=<group_name> --group.status.id=1
# 2019/02/09 01:04:25 group created with ID=1066
```

creating user:
```bash
./accounts user.create --user.group.id=1066 --user.login=builder --user.name="builder microservice" --user.password="<user.password>" --user.role="root" --user.status.id=2 --user.phone=12345678
# 2019/02/09 01:15:19 user created with ID=1773
```

running http server:
```bash
go run ./http/main.go
``` 