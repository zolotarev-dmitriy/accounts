package models

import (
	"database/sql"

	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"

	"github.com/toolsparty/mvc"
)

var baseModel = &mvc.BaseModel{}

func Models(db *sql.DB) []mvc.Model {
	return []mvc.Model{
		postgres.UserService(db, baseModel),
		postgres.GroupService(db, baseModel),
		postgres.TokenService(db, baseModel),
		postgres.SourceService(db, baseModel),
	}
}
