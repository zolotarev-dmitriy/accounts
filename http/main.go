package main

import (
	"context"
	"database/sql"
	"log"

	_ "github.com/lib/pq"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/controllers"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/models"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/views"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"

	"github.com/getsentry/raven-go"
	"github.com/spf13/pflag"
	"github.com/toolsparty/mvc"
	router "github.com/toolsparty/mvc-router"
)

var (
	confPath = pflag.String("confPath", "./config.yaml", "Configuration file")
)

func main() {
	config, err := helpers.ReadConfig(confPath)
	if err != nil {
		log.Fatalf("config error: %s", err)
	}

	if sentryDSN := config.GetString("sentry.dsn"); sentryDSN != "" {
		err = raven.SetDSN(sentryDSN)
		if err != nil {
			log.Fatalf("sentry error: %s", err)
		}

		log.Println("sentry initialized")
	}

	db, err := sql.Open(config.GetString("database.driver"), config.GetString("database.prod.dsn"))
	if err != nil {
		log.Fatalf("database error: %s", err)
	}

	postgres.TokenLifeTime = config.GetDuration("http.token_life_time")

	route := &router.Router{}

	app, err := mvc.CreateApp(&mvc.AppConfig{
		Context:     context.Background(),
		Router:      route,
		Config:      config,
		Controllers: controllers.Controllers,
		Models:      models.Models(db),
		Views:       views.Views,

		Logger: func(args ...interface{}) {
			for _, arg := range args {
				if err, ok := arg.(*components.HttpError); ok {
					log.Println("http error:", err.Error(), err.Message())
					continue
				}

				if err, ok := arg.(error); ok {
					raven.CaptureError(err, nil)
					log.Println("error:", arg)
					continue
				}

				log.Println(arg)
			}
		},
	})
	if err != nil {
		log.Fatalf("creating app failed: %s", err)
	}

	err = app.Run()
	if err != nil {
		log.Fatalf("running app failed: %s", err)
	}
}
