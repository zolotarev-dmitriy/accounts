package components

import "net/http"

const (
	GetContextFailed = "getting context failed"
)

type HttpError struct {
	code    int
	message string
	err     error
}

func (e HttpError) Code() int {
	return e.code
}

func (e HttpError) Error() string {
	if e.err != nil {
		return e.err.Error()
	}

	return e.message
}

func (e HttpError) Message() string {
	return e.message
}

func (e *HttpError) WithDetails(err error) *HttpError {
	e.err = err
	return e
}

func NotFound(message string) *HttpError {
	return &HttpError{
		code:    http.StatusNotFound,
		message: message,
	}
}

func IsNotFound(err error) bool {
	switch err.(type) {
	case HttpError:
		e := err.(HttpError)
		return e.code == http.StatusNotFound
	case *HttpError:
		e := err.(*HttpError)
		return e.code == http.StatusNotFound
	}

	return false
}

func BadRequest(message string) *HttpError {
	return &HttpError{
		code:    http.StatusBadRequest,
		message: message,
	}
}

func Forbidden(message string) *HttpError {
	return &HttpError{
		code:    http.StatusForbidden,
		message: message,
	}
}

func Unauthorized(message string) *HttpError {
	return &HttpError{
		code:    http.StatusUnauthorized,
		message: message,
	}
}

func Locked(message string) *HttpError {
	return &HttpError{
		code:    http.StatusLocked,
		message: message,
	}
}
