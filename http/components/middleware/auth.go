package middleware

import (
	"context"
	"log"
	"strings"
	"time"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"

	"github.com/pkg/errors"
	"github.com/toolsparty/mvc"
	"github.com/valyala/fasthttp"
)

var XAuthTokenName = "X-Auth-Token"
var UserContextKey = userContext{Name: "user_context"}
var UserValueKey = "user_value"
var TokenValueKey = "token_value"

type userContext struct {
	Name string
}

func Auth(c context.Context, action mvc.Action) mvc.Action {
	return func(v interface{}) error {
		ctx, ok := v.(*fasthttp.RequestCtx)
		if !ok {
			return errors.New(components.GetContextFailed)
		}

		tokens := postgres.TokenService(nil, nil)

		t := ctx.Request.Header.Peek(XAuthTokenName)
		if len(t) == 0 {
			return components.BadRequest(XAuthTokenName + " required")
		}

		todo := context.TODO()

		tr, err := tokens.Executor.BeginTx(todo, nil)
		if err != nil {
			return errors.Wrap(err, postgres.CreatingTransactionFailed)
		}
		defer func() {
			err := layers.ExecuteTransaction(tr, err)
			if err != nil {
				if tokens.App != nil {
					tokens.App.Log(err)
					return
				}

				log.Println("execute transaction failed", err)
			}
		}()

		todo = context.WithValue(todo, layers.ExecutorKey, tr)

		token, err := tokens.Get(todo, string(t))
		if err != nil {
			if strings.Contains(err.Error(), postgres.NotFound) {
				return components.NotFound(err.Error())
			}

			return errors.Wrap(err, "getting token failed")
		}
		if token == nil {
			return components.Unauthorized("token not found")
		}

		if token.Status != domain.AuthStatusNew && token.Status != domain.AuthStatusUsed {
			return components.Forbidden("forbidden").WithDetails(errors.New("token status invalid"))
		}

		if token.DTExpired.Before(time.Now()) {
			return components.Locked("expired")
		}

		token, err = tokens.Update(todo, string(t), domain.AuthStatusUsed)
		if err != nil {
			return errors.Wrap(err, "updating token failed")
		}

		ctx.SetUserValue(UserValueKey, token.User)
		ctx.SetUserValue(TokenValueKey, token)

		return action(ctx)
	}
}
