package components

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	"github.com/pkg/errors"
	"github.com/toolsparty/mvc"
	"github.com/valyala/fasthttp"
)

type JSONController struct {
	*mvc.BaseController

	view mvc.View
}

func (b *JSONController) Init() error {
	return b.BaseController.Init()
}

func (JSONController) Context(v interface{}) (*fasthttp.RequestCtx, error) {
	ctx, ok := v.(*fasthttp.RequestCtx)
	if !ok {
		return nil, errors.New(fmt.Sprintf("%T is not *fasthttp.RequestCtx", v))
	}

	return ctx, nil
}

func (b JSONController) Render(w io.Writer, v interface{}) error {
	b.SetHeaders(w)

	encoder := json.NewEncoder(w)
	err := encoder.Encode(v)
	if err != nil {
		return errors.Wrap(err, "encoding to json failed")
	}

	return nil
}

func (JSONController) Decode(b []byte, v interface{}) error {
	decoder := json.NewDecoder(bytes.NewReader(b))
	err := decoder.Decode(v)
	if err != nil {
		return errors.Wrap(err, "decoding json failed")
	}

	return nil
}

func (JSONController) SetHeaders(w io.Writer) {
	wr, ok := w.(*fasthttp.RequestCtx)
	if ok {
		wr.SetContentType("application/json")
	}
}
