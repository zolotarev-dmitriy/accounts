package views

import (
	"io"
	"net/http"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components"

	"github.com/pkg/errors"
	"github.com/toolsparty/mvc"
	"github.com/valyala/fasthttp"
)

type ErrorView struct {
	*mvc.BaseView
}

func (ErrorView) Name() (string, error) {
	return "error", nil
}

func (view *ErrorView) Render(w io.Writer, tpl string, params mvc.ViewParams) error {
	response := &errResponse{
		Code:    http.StatusInternalServerError,
		Message: "Internal Server Error",
		Details: "Error Not Found",
	}

	wr, ok := w.(*fasthttp.RequestCtx)
	if ok {
		defer func() {
			wr.SetContentType("application/json")
			wr.SetStatusCode(response.Code)
		}()
	}

	err, ok := params["error"].(error)
	if !ok {
		if err := helpers.ToJSON(w, response); err != nil {
			return errors.Wrap(err, "encoding to json failed")
		}

		return errors.New("error not found")
	}

	e, ok := err.(*components.HttpError)
	if !ok {
		response.Details = err.Error()
		return helpers.ToJSON(w, response)
	}

	response.Code = e.Code()
	response.Message = e.Message()
	response.Details = e.Error()

	return helpers.ToJSON(w, response)
}

type errResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Details string `json:"details,omitempty"`
}
