package views

import "github.com/toolsparty/mvc"

var baseView = &mvc.BaseView{}

var Views = []mvc.View{
	&ErrorView{
		BaseView: baseView,
	},
}
