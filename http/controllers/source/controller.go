package source

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components/middleware"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"

	"github.com/pkg/errors"
	"github.com/toolsparty/mvc"
	"context"
)

type Controller struct {
	*components.JSONController

	users   domain.UserStore
	sources domain.UserSourceStore
}

func (Controller) Name() (string, error) {
	return "source", nil
}

func (c *Controller) Actions() (mvc.Actions, error) {
	return mvc.Actions{
		"GET /user/source/:name/:id":             c.Get,
		"POST /user/source":                      c.Set,
		"DELETE /user/source/:name/:id/:user_id": c.Remove,
	}, nil
}

func (c *Controller) Init() error {
	userModel, ok := c.App.Model(domain.UserModelName).(domain.UserStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", userModel))
	}

	c.users = userModel

	sourceModel, ok := c.App.Model(domain.SourceModelName).(domain.UserSourceStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserSourceStore", sourceModel))
	}

	c.sources = sourceModel

	return c.JSONController.Init()
}

func (c *Controller) BeforeAction(action mvc.Action) mvc.Action {
	return middleware.Auth(c.App.Context(), action)
}

func (c *Controller) Get(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	name := ctx.UserValue("name").(string)

	id, err := strconv.ParseInt(ctx.UserValue("id").(string), 10, 64)
	if err != nil {
		return components.BadRequest("bad id").WithDetails(err)
	}

	source, err := c.sources.Get(context.Background(), id, name)
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound(err.Error())
		}

		return errors.Wrap(err, "getting user source failed")
	}

	return c.Render(ctx, source)
}

func (c *Controller) Set(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	source := &domain.UserSource{}

	err = c.Decode(ctx.PostBody(), source)
	if err != nil {
		return components.BadRequest("bad request").WithDetails(err)
	}

	if source.User == nil {
		user, ok := ctx.UserValue(middleware.UserValueKey).(*domain.User)
		if ok {
			source.User = user
		}
	}

	err = c.sources.Set(context.Background(), source)
	if err != nil {
		return errors.Wrap(err, "setting user source failed")
	}

	return c.Render(ctx, source)
}

func (c *Controller) Remove(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	name := ctx.UserValue("name").(string)

	id, err := strconv.ParseInt(ctx.UserValue("id").(string), 10, 64)
	if err != nil {
		return components.BadRequest("bad id").WithDetails(err)
	}

	userID, err := strconv.ParseInt(ctx.UserValue("user_id").(string), 10, 64)
	if err != nil {
		return components.BadRequest("bad user id").WithDetails(err)
	}

	if userID < 0 {
		user, ok := ctx.UserValue(middleware.UserValueKey).(*domain.User)
		if ok {
			userID = int64(user.ID)
		}
	}

	source := &domain.UserSource{
		SourceID: id,
		Source:   name,
		User: &domain.User{
			ID: int(userID),
		},
	}

	err = c.sources.Remove(context.Background(), source)
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound(err.Error())
		}

		return errors.Wrap(err, "deleting user source failed")
	}

	return nil
}
