package user

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components/middleware"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"

	"github.com/pkg/errors"
	"github.com/toolsparty/mvc"
)

const (
	dateTimeLayout = "2006-01-02"
)

type UsersController struct {
	*components.JSONController
}

func (UsersController) Name() (string, error) {
	return "user", nil
}

func (c UsersController) Actions() (mvc.Actions, error) {
	return mvc.Actions{
		"GET /users":        c.List,
		"GET /users/:id":    c.Get,
		"POST /users":       c.Create,
		"PUT /users":        c.Update,
		"DELETE /users/:id": c.Delete,
	}, nil
}

func (c *UsersController) BeforeAction(action mvc.Action) mvc.Action {
	return middleware.Auth(c.App.Context(), action)
}

func (c *UsersController) List(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	filter := &domain.UserFilter{
		Name:  string(ctx.QueryArgs().Peek("name")),
		Login: string(ctx.QueryArgs().Peek("login")),
		Phone: string(ctx.QueryArgs().Peek("phone")),
	}

	if dateType := ctx.Request.URI().QueryArgs().Peek("dt_type"); len(dateType) > 0 {
		dType := domain.DateType(dateType)
		filter.DateType = &dType
	}

	if status := ctx.QueryArgs().Peek("status"); len(status) > 0 {
		userStatus, err := strconv.ParseInt(string(status), 10, 64)
		if err != nil {
			return errors.Wrap(err, "parsing user status failed")
		}

		filter.Status = int(userStatus)
	}

	if start := ctx.QueryArgs().Peek("dt_start"); len(start) > 0 {
		DTStart, err := time.Parse(dateTimeLayout, string(start))
		if err != nil {
			return errors.Wrap(err, "parsing dt_start failed")
		}

		filter.DTStart = DTStart
	}

	if end := ctx.QueryArgs().Peek("dt_end"); len(end) > 0 {
		DTEnd, err := time.Parse(dateTimeLayout, string(end))
		if err != nil {
			return errors.Wrap(err, "parsing dt_start failed")
		}

		filter.DTEnd = DTEnd
	}

	userModel, ok := c.App.Model(domain.UserModelName).(domain.UserStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", userModel))
	}

	users, err := userModel.Search(context.Background(), filter)
	if err != nil {
		return errors.Wrap(err, "search users failed")
	}

	return c.Render(ctx, users)
}

func (c *UsersController) Get(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	userID, err := strconv.ParseInt(ctx.UserValue("id").(string), 10, 64)
	if err != nil {
		return components.BadRequest("bad id").WithDetails(err)
	}

	userModel, ok := c.App.Model(domain.UserModelName).(domain.UserStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", userModel))
	}

	user := &domain.User{
		ID: int(userID),
	}

	err = userModel.GetOne(context.Background(), user)
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound(err.Error())
		}

		return errors.Wrap(err, "getting user failed")
	}

	return c.Render(ctx, user)
}

func (c *UsersController) Create(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	user := &domain.User{}

	err = c.Decode(ctx.PostBody(), user)
	if err != nil {
		return components.BadRequest("bad request").WithDetails(err)
	}

	userModel, ok := c.App.Model(domain.UserModelName).(domain.UserStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", userModel))
	}

	err = userModel.Create(context.Background(), user)
	if err != nil {
		return errors.Wrap(err, "creating user failed")
	}

	return c.Render(ctx, user)
}

func (c *UsersController) Update(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	user := &domain.User{}

	err = c.Decode(ctx.PostBody(), user)
	if err != nil {
		return components.BadRequest("bad request").WithDetails(err)
	}

	userModel, ok := c.App.Model(domain.UserModelName).(domain.UserStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", userModel))
	}

	if user.Password != "" {
		pass, err := helpers.EncryptPassword([]byte(user.Password))
		if err != nil {
			return errors.Wrap(err, "encryption password failed")
		}

		user.Password = string(pass)
	}

	err = userModel.Update(context.Background(), user)
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound(err.Error())
		}

		return errors.Wrap(err, "updating user failed")
	}

	return c.Render(ctx, user)
}

func (c *UsersController) Delete(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	userID, err := strconv.ParseInt(ctx.UserValue("id").(string), 10, 64)
	if err != nil {
		return components.BadRequest("bad id").WithDetails(err)
	}

	userModel, ok := c.App.Model(domain.UserModelName).(domain.UserStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", userModel))
	}

	user := &domain.User{
		ID: int(userID),
	}

	err = userModel.Delete(context.Background(), user)
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound(err.Error())
		}

		return errors.Wrap(err, "updating user failed")
	}

	return nil
}
