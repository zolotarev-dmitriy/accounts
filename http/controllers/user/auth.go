package user

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components/middleware"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"

	"github.com/pkg/errors"
	"github.com/toolsparty/mvc"
)

type AuthController struct {
	*components.JSONController

	tokens domain.AuthTokenStore
	users  domain.UserStore
}

func (AuthController) Name() (string, error) {
	return "auth", nil
}

func (c *AuthController) Init() error {
	if err := c.JSONController.Init(); err != nil {
		return errors.Wrap(err, "json controller error")
	}

	userModel, ok := c.App.Model(domain.UserModelName).(domain.UserStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", userModel))
	}

	c.users = userModel

	tokenModel, ok := c.App.Model(domain.TokenModelName).(domain.AuthTokenStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.AuthTokenStore", tokenModel))
	}

	c.tokens = tokenModel

	return nil
}

func (c *AuthController) Actions() (mvc.Actions, error) {
	return mvc.Actions{
		"GET /auth/token":  middleware.Auth(c.App.Context(), c.AuthByToken),
		"POST /auth":       c.Auth,
		"DELETE /auth":     c.Logout,
		"POST /auth/token": middleware.Auth(c.App.Context(), c.CreateToken),
	}, nil
}

func (c *AuthController) AuthByToken(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	token, ok := ctx.UserValue(middleware.TokenValueKey).(*domain.AuthToken)
	if !ok {
		return components.Unauthorized("user not found")
	}

	return c.Render(ctx, token)
}

func (c *AuthController) Auth(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	req := &domain.User{}

	err = c.Decode(ctx.PostBody(), req)
	if err != nil {
		return components.BadRequest("bad request").WithDetails(err)
	}

	users, err := c.users.Search(context.Background(), &domain.UserFilter{
		Login:   req.Login,
		Exactly: true,
	})
	if err != nil {
		return errors.Wrap(err, "search for users failed")
	}

	if len(users) != 1 {
		return components.NotFound("user not found")
	}

	user := users[0]

	if user.Status.ID != 2 {
		return components.Forbidden("forbidden")
	}

	if err := helpers.ComparePassword(user.Password, req.Password); err != nil {
		return components.Forbidden("").WithDetails(err)
	}

	token, err := c.tokens.Create(context.Background(), user, domain.AuthStatusNew)
	if err != nil {
		return errors.Wrap(err, "creating user token failed")
	}

	return c.Render(ctx, token)
}

func (c *AuthController) Logout(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	t := ctx.Request.Header.Peek("X-Auth-Token")
	if len(t) == 0 {
		return components.BadRequest("bad token")
	}

	err = c.tokens.Delete(context.Background(), string(t))
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound("token not found")
		}

		return errors.Wrap(err, "search token failed")
	}

	return nil
}

func (c *AuthController) CreateToken(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	token, ok := ctx.UserValue(middleware.TokenValueKey).(*domain.AuthToken)
	if !ok {
		return components.Unauthorized("user not found")
	}

	if token.User.Role != domain.RoleOwner && token.User.Role != domain.RoleRoot {
		return components.Forbidden("forbidden")
	}

	req := &domain.AuthToken{}

	err = c.Decode(ctx.PostBody(), req)
	if err != nil {
		return components.BadRequest("bad request").WithDetails(err)
	}

	authToken, err := c.tokens.Create(context.Background(), req.User, req.Status)
	if err != nil {
		return errors.Wrap(err, "creating token failed")
	}

	return c.Render(ctx, authToken)
}
