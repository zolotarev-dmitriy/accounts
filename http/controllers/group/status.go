package group

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components/middleware"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"

	"github.com/pkg/errors"
	"github.com/toolsparty/mvc"
	"context"
)

type StatusController struct {
	*components.JSONController
}

func (StatusController) Name() (string, error) {
	return "group_status", nil
}

func (c *StatusController) Actions() (mvc.Actions, error) {
	return mvc.Actions{
		"GET /group/statuses":        c.List,
		"GET /group/statuses/:id":    c.Get,
		"POST /group/statuses":       c.Create,
		"PUT /group/statuses":        c.Update,
		"DELETE /group/statuses/:id": c.Delete,
	}, nil
}

func (c *StatusController) BeforeAction(action mvc.Action) mvc.Action {
	return middleware.Auth(c.App.Context(), action)
}

func (c *StatusController) List(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	groupModel, ok := c.App.Model(domain.GroupModelName).(domain.GroupStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", groupModel))
	}

	statuses, err := groupModel.StatusList(context.Background())
	if err != nil {
		return errors.Wrap(err, "search for user statuses failed")
	}

	return c.Render(ctx, statuses)
}

func (c *StatusController) Get(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	statusID, err := strconv.ParseInt(ctx.UserValue("id").(string), 10, 64)
	if err != nil {
		return components.BadRequest("bad id").WithDetails(err)
	}

	groupModel, ok := c.App.Model(domain.GroupModelName).(domain.GroupStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", groupModel))
	}

	statuses, err := groupModel.StatusList(context.Background())
	if err != nil {
		return errors.Wrap(err, "search for user statuses failed")
	}

	for _, status := range statuses {
		if int(statusID) == status.ID {
			return c.Render(ctx, status)
		}
	}

	return components.NotFound("status not found")
}

func (c *StatusController) Create(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	status := &domain.GroupStatus{}

	err = c.Decode(ctx.PostBody(), status)
	if err != nil {
		return components.BadRequest("bad request").WithDetails(err)
	}

	groupModel, ok := c.App.Model(domain.GroupModelName).(domain.GroupStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", groupModel))
	}

	err = groupModel.CreateStatus(context.Background(), status)
	if err != nil {
		return errors.Wrap(err, "creating user status failed")
	}

	return c.Render(ctx, status)
}

func (c *StatusController) Update(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	status := &domain.GroupStatus{}

	err = c.Decode(ctx.PostBody(), status)
	if err != nil {
		return components.BadRequest("bad request").WithDetails(err)
	}

	groupModel, ok := c.App.Model(domain.GroupModelName).(domain.GroupStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", groupModel))
	}

	err = groupModel.UpdateStatus(context.Background(), status)
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound(err.Error())
		}

		return errors.Wrap(err, "updating user status failed")
	}

	return c.Render(ctx, status)
}

func (c *StatusController) Delete(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	statusID, err := strconv.ParseInt(ctx.UserValue("id").(string), 10, 64)
	if err != nil {
		return components.BadRequest("bad id").WithDetails(err)
	}

	status := &domain.GroupStatus{
		ID: int(statusID),
	}

	groupModel, ok := c.App.Model(domain.GroupModelName).(domain.GroupStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.UserStore", groupModel))
	}

	err = groupModel.DeleteStatus(context.Background(), status)
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound(err.Error())
		}

		return errors.Wrap(err, "deleting user status failed")
	}

	return nil
}
