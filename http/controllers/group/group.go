package group

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components/middleware"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"

	"github.com/pkg/errors"
	"github.com/toolsparty/mvc"
)

type GroupsController struct {
	*components.JSONController
}

func (GroupsController) Name() (string, error) {
	return "group", nil
}

func (c *GroupsController) Actions() (mvc.Actions, error) {
	return mvc.Actions{
		"GET /groups":        c.List,
		"GET /groups/:id":    c.Get,
		"POST /groups":       c.Create,
		"PUT /groups":        c.Update,
		"DELETE /groups/:id": c.Delete,
	}, nil
}

func (c *GroupsController) BeforeAction(action mvc.Action) mvc.Action {
	return middleware.Auth(c.App.Context(), action)
}

func (c *GroupsController) List(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	groupModel, ok := c.App.Model(domain.GroupModelName).(domain.GroupStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.GroupStore", groupModel))
	}

	groups, err := groupModel.Search(context.Background())
	if err != nil {
		return errors.Wrap(err, "search for groups failed")
	}

	return c.Render(ctx, groups)
}

func (c *GroupsController) Get(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	groupID, err := strconv.ParseInt(ctx.UserValue("id").(string), 10, 64)
	if err != nil {
		return components.BadRequest("bad id").WithDetails(err)
	}

	groupModel, ok := c.App.Model(domain.GroupModelName).(domain.GroupStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.GroupStore", groupModel))
	}

	group := &domain.Group{
		ID: int(groupID),
	}

	err = groupModel.GetOne(context.Background(), group)
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound(err.Error())
		}

		return errors.Wrap(err, "getting group failed")
	}

	return c.Render(ctx, group)
}

func (c *GroupsController) Create(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	group := &domain.Group{}

	err = c.Decode(ctx.PostBody(), group)
	if err != nil {
		return components.BadRequest("bad request").WithDetails(err)
	}

	groupModel, ok := c.App.Model(domain.GroupModelName).(domain.GroupStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.GroupStore", groupModel))
	}

	err = groupModel.Create(context.Background(), group)
	if err != nil {
		return errors.Wrap(err, "creating group failed")
	}

	return c.Render(ctx, group)
}

func (c *GroupsController) Update(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	group := &domain.Group{}

	err = c.Decode(ctx.PostBody(), group)
	if err != nil {
		return components.BadRequest("bad request").WithDetails(err)
	}

	groupModel, ok := c.App.Model(domain.GroupModelName).(domain.GroupStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.GroupStore", groupModel))
	}

	err = groupModel.Update(context.Background(), group)
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound(err.Error())
		}

		return errors.Wrap(err, "updating group failed")
	}

	return c.Render(ctx, group)
}

func (c *GroupsController) Delete(v interface{}) error {
	ctx, err := c.Context(v)
	if err != nil {
		return errors.Wrap(err, components.GetContextFailed)
	}

	groupID, err := strconv.ParseInt(ctx.UserValue("id").(string), 10, 64)
	if err != nil {
		return components.BadRequest("bad id").WithDetails(err)
	}

	groupModel, ok := c.App.Model(domain.GroupModelName).(domain.GroupStore)
	if !ok {
		return errors.New(fmt.Sprintf("model %T is not implements interface domain.GroupStore", groupModel))
	}

	group := &domain.Group{
		ID: int(groupID),
	}

	err = groupModel.Delete(context.Background(), group)
	if err != nil {
		if strings.Contains(err.Error(), postgres.NotFound) {
			return components.NotFound(err.Error())
		}

		return errors.Wrap(err, "deleting group failed")
	}

	return nil
}
