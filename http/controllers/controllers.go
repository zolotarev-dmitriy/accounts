package controllers

import (
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/controllers/group"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/controllers/source"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/controllers/user"

	"github.com/toolsparty/mvc"
)

var baseCtrl = &mvc.BaseController{}

var jsonCtrl = &components.JSONController{
	BaseController: baseCtrl,
}

var Controllers = []mvc.Controller{
	&user.AuthController{
		JSONController: jsonCtrl,
	},
	&user.UsersController{
		JSONController: jsonCtrl,
	},
	&user.StatusController{
		JSONController: jsonCtrl,
	},
	&group.GroupsController{
		JSONController: jsonCtrl,
	},
	&group.StatusController{
		JSONController: jsonCtrl,
	},
	&source.Controller{
		JSONController: jsonCtrl,
	},
}
