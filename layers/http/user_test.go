package http

import (
	"testing"
)

func TestClient_ListUsers(t *testing.T) {
	t.Parallel()
	client := getTestClient(t)
	defer client.Logout(client.token)

	_, err := client.ListUsers(client.token, nil)
	if err != nil {
		t.Fatalf("getting groups failed: %s", err)
	}
}
