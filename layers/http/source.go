package http

import (
	"bytes"
	"fmt"
	"net/http"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"

	"github.com/pkg/errors"
)

func (c *Client) GetSource(token string, id int64, name string) (*domain.UserSource, error) {
	uri := c.getURL(
		fmt.Sprintf("%s/%s/%d", SourcePath, name, id),
	)

	source := &domain.UserSource{}

	err := c.Request(http.MethodGet, token, uri, nil, source)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return source, nil
}

func (c *Client) SetSource(token string, source domain.UserSource) (*domain.UserSource, error) {
	buf := &bytes.Buffer{}

	err := helpers.ToJSON(buf, source)
	if err != nil {
		return nil, errors.Wrap(err, JsonErr)
	}

	uri := c.getURL(SourcePath)

	result := &domain.UserSource{}

	err = c.Request(http.MethodPost, token, uri, buf, result)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return result, nil
}

func (c *Client) RemoveSource(token string, id, userID int64, name string) error {
	uri := c.getURL(
		fmt.Sprintf("%s/%s/%d/%d", SourcePath, name, id, userID),
	)

	err := c.Request(http.MethodDelete, token, uri, nil, nil)
	if err != nil {
		return errors.Wrap(err, RequestErr)
	}

	return nil
}
