package http

import (
	"bytes"
	"net/http"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"

	"github.com/pkg/errors"
)

func (c *Client) Auth(login, password string) (*domain.AuthToken, error) {
	buf := &bytes.Buffer{}

	err := helpers.ToJSON(buf, &domain.User{
		Login:    login,
		Password: password,
	})
	if err != nil {
		return nil, errors.Wrap(err, JsonErr)
	}

	uri := c.getURL(AuthPath)

	token := &domain.AuthToken{}

	err = c.Request(http.MethodPost, "", uri, buf, token)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return token, nil
}

func (c *Client) AuthByToken(token string) (*domain.AuthToken, error) {
	uri := c.getURL(AuthByTokenPath)

	authToken := &domain.AuthToken{}

	err := c.Request(http.MethodGet, token, uri, nil, authToken)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return authToken, nil
}

func (c *Client) Logout(token string) error {
	uri := c.getURL(AuthPath)

	err := c.Request(http.MethodDelete, token, uri, nil, nil)
	if err != nil {
		return errors.Wrap(err, RequestErr)
	}

	return nil
}

func (c *Client) CreateToken(token string, authToken domain.AuthToken) (*domain.AuthToken, error) {
	buf := &bytes.Buffer{}

	err := helpers.ToJSON(buf, &authToken)
	if err != nil {
		return nil, errors.Wrap(err, JsonErr)
	}

	uri := c.getURL(AuthByTokenPath)

	auth := &domain.AuthToken{}

	err = c.Request(http.MethodPost, token, uri, buf, auth)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return auth, nil
}
