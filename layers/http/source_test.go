package http

import (
	"math/rand"
	"testing"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"github.com/asticode/go-astitools/string"
)

func TestClient_GetSource(t *testing.T) {
	t.Parallel()
	client := getTestClient(t)
	client.Auth(client.config.Login, client.config.Password)
	defer client.Logout(client.token)

	user := &domain.User{ID: 1657}
	testSource := getTestSource(user)

	source, err := client.SetSource(client.token, *testSource)
	if err != nil {
		t.Fatalf("set user source failed: %s", err)
	}
	defer func() {
		err = client.RemoveSource(client.token, source.SourceID, int64(user.ID), source.Source)
		if err != nil {
			t.Fatalf("remove user source failed: %s", err)
		}
	}()

	if source.User.ID != testSource.User.ID {
		t.Errorf("wrong user id: expected %d, got %d", testSource.User.ID, source.User.ID)
	}
	if source.Source != testSource.Source {
		t.Errorf("wrong user id: expected %s, got %s", testSource.Source, source.Source)
	}
	if source.SourceID != testSource.SourceID {
		t.Errorf("wrong user id: expected %d, got %d", testSource.SourceID, source.SourceID)
	}
	if source.Policy.String() != testSource.Policy.String() {
		t.Errorf("wrong user id: expected %s, got %s", testSource.Policy.String(), source.Policy.String())
	}

	newSource, err := client.GetSource(client.token, source.SourceID, source.Source)
	if err != nil {
		t.Fatalf("getting user source failed: %s", err)
	}

	if source.User.ID != newSource.User.ID {
		t.Errorf("wrong user id: expected %d, got %d", source.User.ID, newSource.User.ID)
	}
	if source.Source != newSource.Source {
		t.Errorf("wrong user id: expected %s, got %s", source.Source, newSource.Source)
	}
	if source.SourceID != newSource.SourceID {
		t.Errorf("wrong user id: expected %d, got %d", source.SourceID, newSource.SourceID)
	}
	if source.Policy.String() != newSource.Policy.String() {
		t.Errorf("wrong user id: expected %s, got %s", source.Policy.String(), newSource.Policy.String())
	}
}

func getTestSource(user *domain.User) *domain.UserSource {
	return &domain.UserSource{
		User:     user,
		SourceID: rand.Int63n(100000),
		Source:   astistring.RandomString(12),
		Policy:   domain.PolicyFromString("0644"),
	}
}
