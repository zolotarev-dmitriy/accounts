package http

import (
	"bytes"
	"fmt"
	"net/http"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"

	"github.com/pkg/errors"
)

func (c *Client) ListGroups(token string) ([]*domain.Group, error) {
	var groups []*domain.Group

	uri := c.getURL(GroupPath)

	err := c.Request(http.MethodGet, token, uri, nil, &groups)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return groups, nil
}

func (c *Client) GetGroup(token string, id int) (*domain.Group, error) {
	uri := c.getURL(fmt.Sprintf("%s/%d", GroupPath, id))

	group := &domain.Group{}

	err := c.Request(http.MethodGet, token, uri, nil, group)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return group, nil
}

func (c *Client) CreateGroup(token string, group domain.Group) (*domain.Group, error) {
	buf := &bytes.Buffer{}

	err := helpers.ToJSON(buf, group)
	if err != nil {
		return nil, errors.Wrap(err, JsonErr)
	}

	uri := c.getURL(GroupPath)

	result := &domain.Group{}

	err = c.Request(http.MethodPost, token, uri, buf, result)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return result, nil
}

func (c *Client) UpdateGroup(token string, group domain.Group) (*domain.Group, error) {
	buf := &bytes.Buffer{}

	err := helpers.ToJSON(buf, group)
	if err != nil {
		return nil, errors.Wrap(err, JsonErr)
	}

	uri := c.getURL(GroupPath)

	result := &domain.Group{}

	err = c.Request(http.MethodPut, token, uri, buf, result)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return result, nil
}

func (c *Client) DeleteGroup(token string, id int) error {
	uri := c.getURL(fmt.Sprintf("%s/%d", GroupPath, id))

	err := c.Request(http.MethodDelete, token, uri, nil, nil)
	if err != nil {
		return errors.Wrap(err, RequestErr)
	}

	return nil
}

func (c *Client) ListGroupStatuses(token string) ([]*domain.GroupStatus, error) {
	var statuses []*domain.GroupStatus

	uri := c.getURL(GroupStatusesPath)

	err := c.Request(http.MethodGet, token, uri, nil, &statuses)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return statuses, nil
}
