package http

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"

	"github.com/pkg/errors"
)

func (c *Client) ListUsers(token string, filter *domain.UserFilter) ([]*domain.User, error) {
	uri := c.getURL(UserPath)

	if filter != nil {
		userFilterToUriParams(uri, filter)
	}

	var users []*domain.User

	err := c.Request(http.MethodGet, token, uri, nil, &users)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return users, nil
}

func (c *Client) GetUser(token string, id int) (*domain.User, error) {
	uri := c.getURL(fmt.Sprintf("%s/%d", UserPath, id))

	user := &domain.User{}

	err := c.Request(http.MethodGet, token, uri, nil, user)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return user, nil
}

func (c *Client) CreateUser(token string, user domain.User) (*domain.User, error) {
	buf := &bytes.Buffer{}

	err := helpers.ToJSON(buf, user)
	if err != nil {
		return nil, errors.Wrap(err, JsonErr)
	}

	uri := c.getURL(UserPath)

	result := &domain.User{}

	err = c.Request(http.MethodPost, token, uri, buf, result)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return result, nil
}

func (c *Client) UpdateUser(token string, user domain.User) (*domain.User, error) {
	buf := &bytes.Buffer{}

	err := helpers.ToJSON(buf, user)
	if err != nil {
		return nil, errors.Wrap(err, JsonErr)
	}

	uri := c.getURL(UserPath)

	result := &domain.User{}

	err = c.Request(http.MethodPut, token, uri, buf, result)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return result, nil
}

func (c *Client) DeleteUser(token string, id int) error {
	uri := c.getURL(fmt.Sprintf("%s/%d", UserPath, id))

	//user := &domain.User{}

	err := c.Request(http.MethodDelete, token, uri, nil, nil)
	if err != nil {
		return errors.Wrap(err, RequestErr)
	}

	return nil
}

func (c *Client) ListUserStatuses(token string) ([]*domain.UserStatus, error) {
	var statuses []*domain.UserStatus

	uri := c.getURL(UserStatusesPath)

	err := c.Request(http.MethodGet, token, uri, nil, &statuses)
	if err != nil {
		return nil, errors.Wrap(err, RequestErr)
	}

	return statuses, nil
}

func userFilterToUriParams(uri *url.URL, filter *domain.UserFilter) {
	if filter == nil || uri == nil {
		return
	}

	params := make(url.Values)

	if filter.Name != "" {
		params.Add("name", filter.Name)
	}
	if filter.Login != "" {
		params.Add("login", filter.Login)
	}
	if filter.Phone != "" {
		params.Add("phone", filter.Phone)
	}
	if filter.Status > 0 {
		params.Add("status", strconv.Itoa(filter.Status))
	}
	if filter.Exactly {
		params.Add("exactly", strconv.FormatBool(filter.Exactly))
	}

	uri.RawQuery = params.Encode()
}
