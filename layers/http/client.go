package http

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"
	"bitbucket.org/zolotarev-dmitriy/accounts/http/components/middleware"

	"github.com/pkg/errors"
)

const (
	AuthPath          = "/auth"
	AuthByTokenPath   = "/auth/token"
	GroupPath         = "/groups"
	GroupStatusesPath = "/group/statuses"
	UserPath          = "/users"
	UserStatusesPath  = "/user/statuses"
	SourcePath        = "/user/source"
)

type Config struct {
	URL             string
	Login, Password string
	AccessToken     string
}

type Client struct {
	config *Config
	client *http.Client
	url    *url.URL
	token  string
}

func (c *Client) Request(method, token string, url *url.URL, body io.Reader, dst interface{}) error {
	req, err := http.NewRequest(method, url.String(), body)
	if err != nil {
		return err
	}

	if method != http.MethodGet {
		req.Header.Add("Content-Type", "application/json")
	}

Request:
	req.Header.Add(middleware.XAuthTokenName, token)

	response, err := c.client.Do(req)
	if err != nil {
		return errors.Wrap(err, "response error")
	}

	defer response.Body.Close()

	if response.StatusCode == http.StatusLocked {
		token, err := c.Auth(c.config.Login, c.config.Password)
		if err != nil {
			return errors.Wrap(err, AuthErr)
		}

		c.token = token.Value
		goto Request
	}

	if response.StatusCode != http.StatusOK {
		e := &struct {
			Code    int    `json:"code"`
			Message string `json:"message"`
			Details string `json:"details,omitempty"`
		}{}

		b, _ := ioutil.ReadAll(response.Body)

		err := helpers.FromJSON(bytes.NewReader(b), e)
		if err != nil {
			return errors.Wrap(err, JsonErr+" status")
		}

		return errors.New(
			fmt.Sprintf(
				ServiceErr,
				url.String(), response.StatusCode, e.Message, e.Details,
			),
		)
	}

	if dst != nil {
		err = helpers.FromJSON(response.Body, dst)
		if err != nil {
			return errors.Wrap(err, JsonErr)
		}
	}

	return nil
}

func (c *Client) getURL(path string) *url.URL {
	uri := *c.url
	uri.Path += path

	return &uri
}

func (c *Client) Token() string {
	return c.token
}

func New(config *Config) (*Client, error) {
	uri, err := url.Parse(config.URL)
	if err != nil {
		return nil, errors.Wrap(err, ConfigErr)
	}

	client := &Client{
		config: config,
		client: &http.Client{},
		url:    uri,
		token:  config.AccessToken,
	}

	if config.AccessToken == "" {
		token, err := client.Auth(config.Login, config.Password)
		if err != nil {
			return nil, errors.Wrap(err, AuthErr)
		}

		client.token = token.Value
	}

	return client, nil
}
