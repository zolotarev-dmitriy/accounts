package http

import (
	"net/url"
	"testing"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
)

func TestClient_Auth(t *testing.T) {
	t.Parallel()

	client := getTestClient(t)

	tok, err := client.Auth(client.config.Login, client.config.Password)
	if err != nil {
		t.Fatalf("auth failed: %s", err)
	}

	token := tok.Value
	client.token = token

	defer func() {
		err = client.Logout(token)
		if err != nil {
			t.Errorf("logout failed: %s", err)
		}
	}()

	authToken, err := client.AuthByToken(token)
	if err != nil {
		t.Errorf("auth by token failed: %s", err)
	}

	if tok.ID != authToken.ID {
		t.Errorf(
			"wrong token id: expected %d, got %d",
			tok.ID, authToken.ID,
		)
	}

	authToken.Status = domain.AuthStatusReset

	_, err = client.CreateToken(client.token, *authToken)
	if err != nil {
		t.Fatalf("creating auth token failed: %s", err)
	}
}

func getTestClient(t *testing.T) *Client {
	uri, _ := url.Parse("http://localhost:8089")

	client, err := New(&Config{
		Login:    "admin",
		Password: "123456",
		URL:      uri.String(),
	})
	if err != nil {
		t.Fatalf("creating client failed: %s", err)
	}

	return client
}
