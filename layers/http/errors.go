package http

const (
	ConfigErr   = "config error"
	AuthErr     = "auth failed"
	ResponseErr = "response has errors"
	ServiceErr  = "request %s returns status %d with message \"%s\" and details \"%s\""
	JsonErr     = "json error"
	RequestErr  = "request failed"
)
