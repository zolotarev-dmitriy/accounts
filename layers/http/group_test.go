package http

import (
	"testing"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"github.com/asticode/go-astitools/string"
)

func TestClient_ListGroups(t *testing.T) {
	t.Parallel()
	client := getTestClient(t)
	defer client.Logout(client.token)

	_, err := client.ListGroups(client.token)
	if err != nil {
		t.Fatalf("getting groups failed: %s", err)
	}
}

func TestClient_GetGroup(t *testing.T) {
	t.Parallel()
	client := getTestClient(t)
	defer client.Logout(client.token)

	groups, err := client.ListGroups(client.token)
	if err != nil {
		t.Fatalf("getting groups failed: %s", err)
	}

	groupID := groups[0].ID

	group, err := client.GetGroup(client.token, groupID)
	if err != nil {
		t.Fatalf("getting group failed: %s", err)
	}

	if group.ID != groupID {
		t.Errorf("wrong group id: expected %d, got %d", groupID, group.ID)
	}
}

func TestClient_CreateGroup(t *testing.T) {
	t.Parallel()
	client := getTestClient(t)
	defer client.Logout(client.token)

	testGroup := getTestGroup()

	group, err := client.CreateGroup(client.token, *testGroup)
	if err != nil {
		t.Fatalf("creating group failed: %s", err)
	}
	defer client.DeleteGroup(client.token, group.ID)

	if group.Name != testGroup.Name {
		t.Errorf("wrong group name: expected %s, got %s", testGroup.Name, group.Name)
	}
	if group.Status.ID != testGroup.Status.ID {
		t.Errorf("wrong group id: expected %d, got %d", testGroup.Status.ID, group.Status.ID)
	}

	group.Name = astistring.RandomString(17)
	group.Status.ID = 2

	upGroup, err := client.UpdateGroup(client.token, *group)
	if err != nil {
		t.Fatalf("updating group failed: %s", err)
	}

	if upGroup.Name != group.Name {
		t.Errorf("wrong group name: expected %s, got %s", group.Name, upGroup.Name)
	}
	if group.Status.ID != upGroup.Status.ID {
		t.Errorf("wrong group id: expected %d, got %d", group.Status.ID, upGroup.Status.ID)
	}
}

func getTestGroup() *domain.Group {
	return &domain.Group{
		Name: astistring.RandomString(12),
		Status: &domain.GroupStatus{
			ID: 1,
		},
	}
}
