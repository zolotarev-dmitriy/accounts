package layers

import (
	"context"
	"testing"

	_ "github.com/lib/pq"

	"github.com/volatiletech/sqlboiler/boil"
)

func TestGetExecutor(t *testing.T) {
	t.Parallel()
	ex, err := GetTestExecutor("../config.yaml")
	if err != nil {
		t.Fatalf("getting test executor failed: %s", err)
	}

	ctx, e := GetExecutor(nil, nil)
	if e != nil {
		t.Errorf("getting executor failed: expected nil, got %v", e)
	}
	if ctx == context.Background() {
		t.Errorf("getting context failed: expected %v, got %v", context.Background(), ctx)
	}

	ctx = context.WithValue(ctx, ExecutorKey, ex)

	_, e = GetExecutor(ctx, ex)
	if _, ok := e.(BoilExecutor); !ok && e != ex {
		t.Errorf("wrong executor: expected %v, got %v", ex, e)
	}
}

func TestGetTransactor(t *testing.T) {
	t.Parallel()
	ex, err := GetTestExecutor("../config.yaml")
	if err != nil {
		t.Fatalf("getting test executor failed: %s", err)
	}

	ctx, tr := GetTransactor(nil)
	if tr != nil {
		t.Errorf("getting transactor failed: expected nil, got %v", tr)
	}
	if ctx != context.TODO() {
		t.Errorf("getting context failed: expected %v, got %v", context.Background(), ctx)
	}

	tr, err = ex.BeginTx(ctx, nil)
	if err != nil {
		t.Fatalf("creating transaction failed: %s", err)
	}
	if _, ok := tr.(boil.ContextTransactor); !ok {
		t.Fatalf("transaction is not implements boil.ContextTransactor %v", tr)
	}
	if _, ok := tr.(boil.ContextExecutor); !ok {
		t.Fatalf("transaction is not implements boil.ContextExecutor %v", tr)
	}
	defer tr.Rollback()

	ctx = context.WithValue(ctx, TransactorKey, tr)

	_, tx := GetTransactor(ctx)
	if tx == nil {
		t.Errorf("transactor is nil")
	}
	if _, ok := tx.(boil.ContextExecutor); !ok {
		t.Errorf("transaction is not implements boil.ContextExecutor %v", tx)
	}
	if _, ok := tx.(boil.ContextTransactor); !ok {
		t.Errorf("transaction is not implements boil.ContextExecutor %v", tr)
	}
	if tx != tr {
		t.Errorf("getting transactor failed: expected %v, got %v", tr, tx)
	}
}

func TestExecuteTransaction(t *testing.T) {
	t.Parallel()
	var err error

	ex, err := GetTestExecutor("../config.yaml")
	if err != nil {
		t.Fatalf("getting test executor failed: %s", err)
	}

	ctx := context.TODO()

	tr, err := ex.BeginTx(ctx, nil)
	if err != nil {
		t.Fatalf("creating transaction failed")
	}

	result, err := tr.Exec("select now() as dt")
	if err != nil {
		t.Errorf("executing query failed: %s", err)
	}
	n, err := result.RowsAffected()
	if err != nil {
		t.Errorf("getting affected rows failed: %s", err)
	}
	if n != 1 {
		t.Errorf("wrong count of affected rows: expected 1, got %d", n)
	}

	err = ExecuteTransaction(tr, err)
	if err != nil {
		t.Errorf("executing transaction failed: %s", err)
	}
}
