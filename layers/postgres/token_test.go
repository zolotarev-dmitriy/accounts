package postgres_test

import (
	"testing"
	"time"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"
	"github.com/asticode/go-astitools/string"
)

func TestTokenServiceLayer_All(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.TokenService(db, nil)

	group := getTestGroup()
	postgres.GroupService(db, nil).Create(nil, group)

	user, _ := getTestUser(group)
	postgres.UserService(db, nil).Create(nil, user)

	_, clean := getTestToken(user)
	//defer clean(db, token, t)

	token, err := srv.Create(nil, user, domain.AuthStatusNew)
	if err != nil {
		t.Fatalf("creating user token failed: %s", err)
	}
	defer clean(db, token, t)

	if token.ID == 0 {
		t.Errorf("wrong token id: got %d", token.ID)
	}
	if token.User.ID != user.ID {
		t.Errorf("wrong token user id: expected %d, got %d", user.ID, token.User.ID)
	}
	if token.DTExpired.Sub(token.DTCreated) != time.Hour {
		t.Errorf("wrong token date expired: expected %s, got %s", token.DTCreated.Add(time.Hour), token.DTExpired)
	}
	if n := len(token.Value); n != 60 {
		t.Errorf("wrong token user id: expected %d, got %d", 60, n)
	}
	if token.Status != domain.AuthStatusNew {
		t.Errorf("wrong token status: expected %d, got %d", domain.AuthStatusNew, token.Status)
	}

	tmpToken := *token

	token, err = srv.Update(nil, token.Value, domain.AuthStatusUsed)
	if err != nil {
		t.Fatalf("updating user token failed: %s", err)
	}
	if token.ID != tmpToken.ID {
		t.Errorf("wrong token id: got %d", token.ID)
	}
	if token.User.ID != tmpToken.User.ID {
		t.Errorf("wrong token user id: expected %d, got %d", user.ID, token.User.ID)
	}
	if token.DTExpired.Sub(token.DTCreated) != time.Hour {
		t.Errorf("wrong token date expired: expected %s, got %s", token.DTCreated.Add(time.Hour), token.DTExpired)
	}
	if !token.DTUpdated.After(token.DTCreated) {
		t.Errorf("wrong token date updated: got %s", token.DTUpdated)
	}
	if n := len(token.Value); n != 60 {
		t.Errorf("wrong token user id: expected %d, got %d", 60, n)
	}
	if token.Status != domain.AuthStatusUsed {
		t.Errorf("wrong token status: expected %d, got %d", domain.AuthStatusUsed, token.Status)
	}

	tmpToken = *token

	token, err = srv.Get(nil, token.Value)
	if err != nil {
		t.Fatalf("getting user token failed: %s", err)
	}
	if token == nil {
		t.Fatalf("token %v not found", tmpToken)
	}
	if token.ID != tmpToken.ID {
		t.Errorf("wrong token id: got %d", token.ID)
	}
	if token.User.ID != tmpToken.User.ID {
		t.Errorf("wrong token user id: expected %d, got %d", user.ID, token.User.ID)
	}
	if !token.DTExpired.Equal(tmpToken.DTExpired) {
		t.Errorf("wrong token date expired: expected %s, got %s", tmpToken.DTExpired, token.DTExpired)
	}
	if !token.DTUpdated.Equal(*tmpToken.DTUpdated) {
		t.Errorf("wrong token date updated: expected %s, got %s", *tmpToken.DTUpdated, *token.DTUpdated)
	}
	if !token.DTCreated.Equal(tmpToken.DTCreated) {
		t.Errorf("wrong token date updated: expected %s, got %s", tmpToken.DTCreated, token.DTCreated)
	}
	if token.Status != tmpToken.Status {
		t.Errorf("wrong token status: expected %d, got %d", tmpToken.Status, token.Status)
	}

	err = srv.Delete(nil, token.Value)
	if err != nil {
		t.Fatalf("deleting user token failed: %s", err)
	}
}

type cleanToken func(db layers.BoilExecutor, token *domain.AuthToken, t *testing.T)

func getTestToken(user *domain.User) (*domain.AuthToken, cleanToken) {
	return &domain.AuthToken{
			User:      user,
			Status:    domain.AuthStatusNew,
			Value:     astistring.RandomString(64),
			DTCreated: time.Now(),
			DTExpired: time.Now().Add(time.Second * 1000),
		}, func(db layers.BoilExecutor, token *domain.AuthToken, t *testing.T) {
			_, err := db.Exec("DELETE FROM public.auth_token WHERE id = $1", token.ID)
			if err != nil {
				t.Fatalf("deleting test user token failed: %s", err)
			}

			if token.User.ID > 0 {
				_, err := db.Exec("DELETE FROM public.user WHERE id = $1", token.User.ID)
				if err != nil {
					t.Fatalf("deleting test user failed: %s", err)
				}
			}

			_, err = db.Exec("DELETE FROM public.group WHERE id = $1", token.User.Group.ID)
			if err != nil {
				t.Fatalf("deleting test group %v failed: %s", user.Group, err)
			}
		}
}
