package postgres_test

import (
	"context"
	"math/rand"
	"strings"
	"testing"
	"time"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"
	"github.com/asticode/go-astitools/string"
)

func TestUserService(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv1 := postgres.UserService(db, nil)
	srv2 := postgres.UserService(db, nil)
	if srv1 != srv2 {
		t.Errorf("wrong group service: expected %v, got %v", srv1, srv2)
	}
}

func TestUserServiceLayer_Create(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.UserService(db, nil)
	group := getTestGroup()

	postgres.GroupService(db, nil).Create(nil, group)

	user, clean := getTestUser(group)
	defer clean(db, user, t)

	tmpUser := *user

	err = srv.Create(nil, user)
	if err != nil {
		t.Errorf("creating user failed: %s", err)
	}

	if user.ID == 0 {
		t.Errorf("wrong user id: got %d", user.ID)
	}
	if user.Group.ID != group.ID {
		t.Errorf("wrong user group id: expected %d, got %d", group.ID, user.Group.ID)
	}
	if user.Name != tmpUser.Name {
		t.Errorf("wrong user name: expected %s, got %s", tmpUser.Name, user.Name)
	}
	if user.Login != tmpUser.Login {
		t.Errorf("wrong user login: expected %s, got %s", tmpUser.Name, user.Name)
	}
	if user.Password != tmpUser.Password {
		t.Errorf("wrong user Password: expected %s, got %s", tmpUser.Password, user.Password)
	}
	if user.Phone != tmpUser.Phone {
		t.Errorf("wrong user Phone: expected %s, got %s", tmpUser.Phone, user.Phone)
	}
	if user.DTCreated.Equal(*tmpUser.DTCreated) {
		t.Errorf("wrong user Phone: expected %s, got %s", tmpUser.DTCreated, user.DTCreated)
	}
}

func TestUserServiceLayer_Update(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	newGroup := getTestGroup()
	postgres.GroupService(db, nil).Create(nil, newGroup)

	newUser, newClean := getTestUser(newGroup)
	defer newClean(db, newUser, t)

	group := getTestGroup()
	postgres.GroupService(db, nil).Create(nil, group)

	user, clean := getTestUser(group)
	defer clean(db, user, t)

	ctx := context.TODO()
	tr, err := db.BeginTx(ctx, nil)
	if err != nil {
		t.Fatalf("creating transaction failed: %s", err)
	}
	defer func() {
		err := layers.ExecuteTransaction(tr, err)
		if err != nil {
			t.Fatalf("execute transaction error: %s", err)
		}
	}()

	ctx = context.WithValue(ctx, layers.ExecutorKey, tr)

	srv := postgres.UserService(db, nil)

	err = srv.Create(ctx, user)
	if err != nil {
		t.Fatalf("creating user failed: %s", err)
	}

	updated := newUser.DTCreated.Add(time.Second * 100)
	lastLogged := newUser.DTCreated.Add(time.Second * 1100)

	user.Name = newUser.Name
	user.Phone = newUser.Phone
	user.Password = newUser.Password
	user.Login = newUser.Login
	user.DTUpdated = &updated
	user.DTLastLogged = &lastLogged
	user.Group = newUser.Group
	user.Role = domain.RoleRoot

	err = srv.Update(ctx, user)
	if err != nil {
		t.Fatalf("updating group failed: %s", err)
	}
	defer func() {
		user.Group = group
	}()

	if user.ID == 0 {
		t.Errorf("wrong user id: got %d", user.ID)
	}
	if user.Group.ID != newUser.Group.ID {
		t.Errorf("wrong user group id: expected %d, got %d", newUser.Group.ID, user.Group.ID)
	}
	if user.Name != newUser.Name {
		t.Errorf("wrong user name: expected %s, got %s", newUser.Name, user.Name)
	}
	if user.Login != newUser.Login {
		t.Errorf("wrong user login: expected %s, got %s", newUser.Name, user.Name)
	}
	if user.Password != newUser.Password {
		t.Errorf("wrong user Password: expected %s, got %s", newUser.Password, user.Password)
	}
	if user.Phone != newUser.Phone {
		t.Errorf("wrong user Phone: expected %s, got %s", newUser.Phone, user.Phone)
	}
	if user.DTCreated.Equal(*newUser.DTCreated) {
		t.Errorf("wrong user Phone: expected %s, got %s", newUser.DTCreated, user.DTCreated)
	}
	if user.Role != domain.RoleRoot {
		t.Errorf("wrong user role: expected %s, got %s", domain.RoleRoot, user.Role)
	}
}

func TestUserServiceLayer_Delete(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.UserService(db, nil)
	group := getTestGroup()

	postgres.GroupService(db, nil).Create(nil, group)

	user, clean := getTestUser(group)
	defer clean(db, user, t)

	err = srv.Create(nil, user)
	if err != nil {
		t.Errorf("creating user failed: %s", err)
	}

	err = srv.Delete(nil, user)
	if err != nil {
		t.Errorf("deleting user failed: %s", err)
	}

	newUser := &domain.User{
		ID: user.ID,
	}

	err = srv.GetOne(nil, newUser)
	if err != nil && !strings.Contains(err.Error(), postgres.NotFound) {
		t.Fatalf("getting user failed: %s", err)
	}
	if !strings.Contains(err.Error(), postgres.NotFound) {
		t.Errorf("user found (should be not found)")
	}
}

func TestUserServiceLayer_GetOne(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.UserService(db, nil)
	group := getTestGroup()

	postgres.GroupService(db, nil).Create(nil, group)

	user, clean := getTestUser(group)
	defer clean(db, user, t)

	err = srv.Create(nil, user)
	if err != nil {
		t.Fatalf("creating user failed: %s", err)
	}

	newUser := &domain.User{
		ID: user.ID,
	}

	err = srv.GetOne(nil, newUser)
	if err != nil {
		t.Fatalf("getting user failed: %s", err)
	}

	if user.Group.ID != newUser.Group.ID {
		t.Errorf("wrong user group id: expected %d, got %d", newUser.Group.ID, user.Group.ID)
	}
	if user.Name != newUser.Name {
		t.Errorf("wrong user name: expected %s, got %s", newUser.Name, user.Name)
	}
	if user.Login != newUser.Login {
		t.Errorf("wrong user login: expected %s, got %s", newUser.Name, user.Name)
	}
	p, _ := helpers.EncryptPassword([]byte(user.Password))
	if err := helpers.ComparePassword(string(p), newUser.Password); err != nil {
		t.Errorf("wrong user Password: expected %s, got %s", string(p), newUser.Password)
	}
	if user.Phone != newUser.Phone {
		t.Errorf("wrong user Phone: expected %s, got %s", newUser.Phone, user.Phone)
	}
	if user.DTCreated.Equal(*newUser.DTCreated) {
		t.Errorf("wrong user Phone: expected %s, got %s", newUser.DTCreated, user.DTCreated)
	}
}

func TestUserServiceLayer_Search(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	group := getTestGroup()
	postgres.GroupService(db, nil).Create(nil, group)

	users := make([]*domain.User, 20)

	srv := postgres.UserService(db, nil)

	ctx := context.TODO()
	tr, err := db.BeginTx(ctx, nil)
	if err != nil {
		t.Fatalf("creating transaction failed: %s", err)
	}

	ctx = context.WithValue(ctx, layers.ExecutorKey, tr)

	var clean cleanUser
	for i := 0; i < 20; i++ {
		users[i], clean = getTestUser(group)
		err := srv.Create(ctx, users[i])
		if err != nil {
			t.Fatalf("creating user %v failed: %s", users[i], err)
		}
	}
	defer func() {
		_, err := db.Exec("DELETE FROM public.group WHERE id = $1", group.ID)
		if err != nil {
			t.Fatalf("deleting test group %v failed: %s", group, err)
		}
	}()
	defer func() {
		for i := range users {
			users[i].Group.ID = 0
			clean(db, users[i], t)
		}
	}()

	err = layers.ExecuteTransaction(tr, err)
	if err != nil {
		t.Fatalf("execute transaction error: %s", err)
	}

	testUsers, err := srv.Search(nil, nil)
	if err != nil {
		t.Fatalf("getting users failed: %s", err)
	}

	if len(testUsers) != 20 {
		t.Fatalf("wrong count of users: expected 20, got %d", len(testUsers))
	}
}

func TestUserServiceLayer_Status(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.UserService(db, nil)
	status := &domain.UserStatus{
		ID:        10,
		Name:      astistring.RandomString(12),
		SortOrder: 10,
	}
	defer func() {
		_, err := db.Exec("DELETE FROM public.user_status WHERE id = $1", status.ID)
		if err != nil {
			t.Errorf("deleting test user status failed: %s", err)
		}
	}()

	tmpStatus := *status

	err = srv.CreateStatus(nil, status)
	if err != nil {
		t.Fatalf("creating user status failed: %s", err)
	}

	if tmpStatus.ID != status.ID {
		t.Errorf("wrong user status id: expected %d, got %d", tmpStatus.ID, status.ID)
	}
	if tmpStatus.Name != status.Name {
		t.Errorf("wrong user status Name: expected %s, got %s", tmpStatus.Name, status.Name)
	}
	if tmpStatus.SortOrder != status.SortOrder {
		t.Errorf("wrong user status SortOrder: expected %d, got %d", tmpStatus.SortOrder, status.SortOrder)
	}

	status.Name = astistring.RandomString(14)
	status.SortOrder = 14
	tmpStatus = *status

	err = srv.UpdateStatus(nil, status)
	if err != nil {
		t.Fatalf("updating user status failed: %s", err)
	}

	if tmpStatus.ID != status.ID {
		t.Errorf("wrong user status id: expected %d, got %d", tmpStatus.ID, status.ID)
	}
	if tmpStatus.Name != status.Name {
		t.Errorf("wrong user status Name: expected %s, got %s", tmpStatus.Name, status.Name)
	}
	if tmpStatus.SortOrder != status.SortOrder {
		t.Errorf("wrong user status SortOrder: expected %d, got %d", tmpStatus.SortOrder, status.SortOrder)
	}

	statuses, err := srv.StatusList(nil)
	if err != nil {
		t.Fatalf("getting group statuses failed: %s", err)
	}
	if len(statuses) == 0 {
		t.Fatalf("group status not found")
	}

	found := false
	for _, st := range statuses {
		if st.ID == status.ID && st.Name == status.Name && st.SortOrder == status.SortOrder {
			found = true
		}
	}

	if !found {
		t.Fatalf("group status %v not found", status)
	}
}

type cleanUser func(db layers.BoilExecutor, user *domain.User, t *testing.T)

func getTestUser(group *domain.Group) (*domain.User, cleanUser) {
	now := time.Now()
	return &domain.User{
			Group: group,
			Status: &domain.UserStatus{
				ID: 2,
			},
			Name:      astistring.RandomString(12),
			DTCreated: &now,
			Password:  astistring.RandomString(64),
			Phone:     astistring.RandomString(16),
			Role:      domain.RoleUser,
			Login:     astistring.RandomString(24),
			Data: map[string]interface{}{
				astistring.RandomString(6): astistring.RandomString(16),
				astistring.RandomString(4): rand.Intn(999),
			},
		}, func(db layers.BoilExecutor, user *domain.User, t *testing.T) {
			if user.ID > 0 {
				_, err := db.Exec("DELETE FROM public.user WHERE id = $1", user.ID)
				if err != nil {
					t.Fatalf("deleting test user failed: %s", err)
				}
			}

			_, err := db.Exec("DELETE FROM public.group WHERE id = $1", user.Group.ID)
			if err != nil {
				t.Fatalf("deleting test group %v failed: %s", user.Group, err)
			}
		}
}
