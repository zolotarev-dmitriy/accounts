package postgres_test

import (
	"context"
	"math/rand"
	"testing"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"
	"github.com/asticode/go-astitools/string"
)

func TestSourceService(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv1 := postgres.SourceService(db, nil)
	srv2 := postgres.SourceService(db, nil)
	if srv1 != srv2 {
		t.Errorf("wrong group service: expected %v, got %v", srv1, srv2)
	}
}

func TestSourceServiceLayer_Set(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	source, clean := getTestSource()
	defer clean(db, source, t)

	srv := postgres.SourceService(db, nil)

	err = srv.Set(nil, source)
	if err != nil {
		t.Fatalf("creating user source failed: %s", err)
	}
	if source.ID == 0 {
		t.Errorf("wrong source id: got %d", source.ID)
	}

	newSource := *source
	newSource.Policy = domain.PolicyFromString("0754")

	err = srv.Set(nil, &newSource)
	if err != nil {
		t.Fatalf("setting user source failed: %s", err)
	}
	if newSource.ID != source.ID {
		t.Errorf("wrong source id: expected %d, got %d", source.ID, newSource.ID)
	}
}

func TestSourceServiceLayer_Get(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	source, clean := getTestSource()
	defer clean(db, source, t)

	srv := postgres.SourceService(db, nil)

	ctx := context.TODO()
	tr, err := db.BeginTx(ctx, nil)
	if err != nil {
		t.Fatalf("creating transaction failed: %s", err)
	}
	defer func() {
		err := layers.ExecuteTransaction(tr, err)
		if err != nil {
			t.Fatalf("execute transaction error: %s", err)
		}
	}()

	ctx = context.WithValue(ctx, layers.ExecutorKey, tr)

	err = srv.Set(ctx, source)
	if err != nil {
		t.Fatalf("creating user source failed: %s", err)
	}

	newSource, err := srv.Get(ctx, source.SourceID, source.Source)
	if err != nil {
		t.Fatalf("getting user source failed: %s", err)
	}

	if newSource.ID != source.ID {
		t.Errorf("wrong source id: expected %d, got %d", source.ID, newSource.ID)
	}
	if newSource.SourceID != source.SourceID {
		t.Errorf("wrong source source_id: expected %d, got %d", source.SourceID, newSource.SourceID)
	}
	if newSource.Source != source.Source {
		t.Errorf("wrong source name: expected %s, got %s", source.Source, newSource.Source)
	}
	if newSource.User.ID != source.User.ID {
		t.Errorf("wrong source user id: expected %d, got %d", source.User.ID, newSource.User.ID)
	}
	if newSource.Policy.String() != source.Policy.String() {
		t.Errorf("wrong source policy: expected %s, got %s", source.Policy.String(), newSource.Policy.String())
	}
}

func TestSourceServiceLayer_Remove(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	source, clean := getTestSource()
	defer clean(db, source, t)

	srv := postgres.SourceService(db, nil)

	err = srv.Set(nil, source)
	if err != nil {
		t.Fatalf("creating user source failed: %s", err)
	}

	err = srv.Remove(nil, source)
	if err != nil {
		t.Fatalf("removing user source failed: %s", err)
	}

	newSource, err := srv.Get(nil, source.SourceID, source.Source)
	if err != nil {
		t.Fatalf("getting user source failed: %s", err)
	}
	if newSource != nil {
		t.Fatalf("removing source %v failed", newSource)
	}
}

type cleanSource func(db layers.BoilExecutor, source *domain.UserSource, t *testing.T)

func getTestSource() (*domain.UserSource, cleanSource) {
	db, _ := layers.GetTestExecutor("../../config.yaml")

	group := getTestGroup()
	postgres.GroupService(db, nil).Create(nil, group)

	user, clean := getTestUser(group)
	postgres.UserService(db, nil).Create(nil, user)

	return &domain.UserSource{
			User:     user,
			Policy:   domain.PolicyFromString("0777"),
			SourceID: rand.Int63(),
			Source:   astistring.RandomString(12),
		}, func(db layers.BoilExecutor, source *domain.UserSource, t *testing.T) {
			_, err := db.Exec("DELETE FROM public.user_source WHERE id = $1", source.ID)
			if err != nil {
				t.Fatalf("deleting test user source failed: %s", err)
			}

			clean(db, source.User, t)
		}
}
