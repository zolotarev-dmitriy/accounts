package postgres_test

import (
	"context"
	"testing"

	_ "github.com/lib/pq"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"
	"github.com/asticode/go-astitools/string"
)

func TestGroupService(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv1 := postgres.GroupService(db, nil)
	srv2 := postgres.GroupService(db, nil)
	if srv1 != srv2 {
		t.Errorf("wrong group service: expected %v, got %v", srv1, srv2)
	}
}

func TestGroupServiceLayer_Create(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.GroupService(db, nil)
	group := getTestGroup()

	err = srv.Create(nil, group)
	if err != nil {
		t.Errorf("creating group failed: %s", err)
	}
	defer func() {
		_, err := db.Exec("DELETE FROM public.group WHERE \"name\" = $1", group.Name)
		if err != nil {
			t.Fatalf("deleting group failed: %s", err)
		}
	}()

	if group.ID == 0 {
		t.Errorf("wrong group id: got %d", group.ID)
	}
}

func TestGroupServiceLayer_Update(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.GroupService(db, nil)
	group := getTestGroup()
	groupName := astistring.RandomString(20)
	groupStatusID := 2
	defer func() {
		_, err := db.Exec("DELETE FROM public.group WHERE id = $1", group.ID)
		if err != nil {
			t.Errorf("deleting test group failed: %s", err)
		}
	}()

	ctx := context.TODO()
	tr, err := db.BeginTx(ctx, nil)
	if err != nil {
		t.Fatalf("creating transaction failed: %s", err)
	}
	defer func() {
		err := layers.ExecuteTransaction(tr, err)
		if err != nil {
			t.Fatalf("execute transaction error: %s", err)
		}
	}()

	ctx = context.WithValue(ctx, layers.ExecutorKey, tr)

	err = srv.Create(ctx, group)
	if err != nil {
		t.Fatalf("creating group failed: %s", err)
	}

	group.Name = groupName
	group.Status.ID = groupStatusID

	err = srv.Update(ctx, group)
	if err != nil {
		t.Fatalf("updating group failed: %s", err)
	}

	if group.Name != groupName {
		t.Errorf("wrong group name: expected %s, got %s", groupName, group.Name)
	}
	if group.Status.ID != groupStatusID {
		t.Errorf("wrong group status: expected %d, got %d", groupStatusID, group.Status.ID)
	}
}

func TestGroupServiceLayer_Delete(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.GroupService(db, nil)
	group := getTestGroup()

	err = srv.Create(nil, group)
	if err != nil {
		t.Fatalf("creating group failed: %s", err)
	}

	err = srv.Delete(nil, group)
	if err != nil {
		t.Errorf("deleting group failed: %s", err)
	}
}

func TestGroupServiceLayer_GetOne(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.GroupService(db, nil)
	group := getTestGroup()

	err = srv.Create(nil, group)
	if err != nil {
		t.Fatalf("creating group failed: %s", err)
	}
	defer func() {
		_, err := db.Exec("DELETE FROM public.group WHERE id = $1", group.ID)
		if err != nil {
			t.Errorf("deleting test group failed: %s", err)
		}
	}()

	group2 := &domain.Group{
		ID: group.ID,
	}
	err = srv.GetOne(nil, group2)
	if err != nil {
		t.Errorf("getting group failed: %s", err)
	}
}

func TestGroupServiceLayer_Search(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.GroupService(db, nil)
	group1 := getTestGroup()
	group2 := getTestGroup()
	group3 := getTestGroup()

	srv.Create(nil, group1)
	defer func() {
		_, err := db.Exec("DELETE FROM public.group WHERE id = $1", group1.ID)
		if err != nil {
			t.Errorf("deleting test group failed: %s", err)
		}
	}()

	srv.Create(nil, group2)
	defer func() {
		_, err := db.Exec("DELETE FROM public.group WHERE id = $1", group2.ID)
		if err != nil {
			t.Errorf("deleting test group failed: %s", err)
		}
	}()

	srv.Create(nil, group3)
	defer func() {
		_, err := db.Exec("DELETE FROM public.group WHERE id = $1", group3.ID)
		if err != nil {
			t.Errorf("deleting test group failed: %s", err)
		}
	}()

	groups, err := srv.Search(nil)
	if err != nil {
		t.Errorf("search groups failed: %s", err)
	}
	if n := len(groups); n != 3 {
		t.Errorf("wrong count of groups: expected 3, got %d", n)
	}
}

func TestGroupServiceLayer_Status(t *testing.T) {
	db, err := layers.GetTestExecutor("../../config.yaml")
	if err != nil {
		t.Fatalf("getting executor failed: %s", err)
	}

	srv := postgres.GroupService(db, nil)
	status := &domain.GroupStatus{
		ID:        3,
		Name:      astistring.RandomString(14),
		SortOrder: 3,
	}
	defer func() {
		_, err := db.Exec("DELETE FROM public.group_status WHERE id = $1", status.ID)
		if err != nil {
			t.Errorf("deleting test group status failed: %s", err)
		}
	}()

	tmpStatus := *status

	ctx := context.TODO()
	tr, err := db.BeginTx(ctx, nil)
	if err != nil {
		t.Fatalf("creating transaction failed: %s", err)
		return
	}
	defer func() {
		err := layers.ExecuteTransaction(tr, err)
		if err != nil {
			t.Fatalf("execute transaction error: %s", err)
		}
	}()

	ctx = context.WithValue(ctx, layers.ExecutorKey, tr)

	err = srv.CreateStatus(ctx, status)
	if err != nil {
		t.Fatalf("creating group status failed: %s", err)
	}

	if tmpStatus.ID != status.ID {
		t.Errorf("wrong group status id: expected %d, got %d", tmpStatus.ID, status.ID)
	}
	if tmpStatus.Name != status.Name {
		t.Errorf("wrong group status Name: expected %s, got %s", tmpStatus.Name, status.Name)
	}
	if tmpStatus.SortOrder != status.SortOrder {
		t.Errorf("wrong group status SortOrder: expected %d, got %d", tmpStatus.SortOrder, status.SortOrder)
	}

	newName := astistring.RandomString(15)
	newSortOrder := 10

	status.Name = newName
	status.SortOrder = newSortOrder
	tmpStatus = *status

	err = srv.UpdateStatus(ctx, status)
	if err != nil {
		t.Fatalf("updating group status failed: %s", err)
	}
	if tmpStatus.ID != status.ID {
		t.Errorf("wrong group status id: expected %d, got %d", tmpStatus.ID, status.ID)
	}
	if tmpStatus.Name != status.Name {
		t.Errorf("wrong group status Name: expected %s, got %s", tmpStatus.Name, status.Name)
	}
	if tmpStatus.SortOrder != status.SortOrder {
		t.Errorf("wrong group status SortOrder: expected %d, got %d", tmpStatus.SortOrder, status.SortOrder)
	}

	statuses, err := srv.StatusList(ctx)
	if err != nil {
		t.Fatalf("getting group statuses failed: %s", err)
	}
	if len(statuses) == 0 {
		t.Fatalf("group status not found")
	}

	found := false
	for _, st := range statuses {
		if st.ID == status.ID && st.Name == status.Name && st.SortOrder == status.SortOrder {
			found = true
		}
	}

	if !found {
		t.Fatalf("group status %v not found", status)
	}
}

func getTestGroup() *domain.Group {
	group := &domain.Group{
		Name: astistring.RandomString(16),
		Status: &domain.GroupStatus{
			ID: 1,
		},
	}

	return group
}
