package postgres

import (
	"context"
	"database/sql"
	"sync"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers"
	"bitbucket.org/zolotarev-dmitriy/accounts/models/postgres"
	"github.com/toolsparty/mvc"

	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
)

type GroupServiceLayer struct {
	mvc.BaseModel

	Executor layers.BoilExecutor
}

func (GroupServiceLayer) Name() (string, error) {
	return domain.GroupModelName, nil
}

func (srv *GroupServiceLayer) Search(ctx context.Context) ([]*domain.Group, error) {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	groups, err := models.Groups(qm.Load("Status")).All(c, ex)
	if err != nil && err != sql.ErrNoRows {
		return nil, errors.Wrap(err, SearchGroupsFailed)
	}

	results := make([]*domain.Group, len(groups))

	for i, model := range groups {
		results[i] = extractGroup(model, nil)
	}

	return results, nil
}

func (srv *GroupServiceLayer) GetOne(ctx context.Context, group *domain.Group) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if group == nil {
		return errors.New(GroupNotDefined)
	}

	if group.ID == 0 {
		return errors.New(RequiredGroupID)
	}

	model, err := models.Groups(qm.Where("id = ?", group.ID), qm.Load("Status")).One(c, ex)
	if err == sql.ErrNoRows {
		return errors.New(NotFound)
	}
	if err != nil {
		return errors.Wrap(err, SearchGroupFailed)
	}

	extractGroup(model, group)

	return nil
}

func (srv *GroupServiceLayer) Create(ctx context.Context, group *domain.Group) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if group == nil {
		return errors.New(GroupNotDefined)
	}

	if group.Status == nil {
		return errors.New(GroupStatusNotDefined)
	}

	if group.Status.ID == 0 {
		return errors.New(RequiredGroupStatusID)
	}

	model := groupModel(group)

	err := model.Insert(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, InsertFailed)
	}

	extractGroup(model, group)

	return nil
}

func (srv *GroupServiceLayer) Update(ctx context.Context, group *domain.Group) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if group == nil {
		return errors.New(GroupNotDefined)
	}

	if group.ID == 0 {
		return errors.New(RequiredGroupID)
	}

	if group.Status == nil {
		return errors.New(GroupStatusNotDefined)
	}

	if group.Status.ID == 0 {
		return errors.New(RequiredGroupStatusID)
	}

	exists, err := models.Groups(qm.Where("id = ?", group.ID)).Exists(c, ex)
	if err != nil {
		return errors.Wrap(err, SearchGroupFailed)
	}
	if !exists {
		group = nil
		return errors.New(NotFound)
	}

	model := groupModel(group)

	_, err = model.Update(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, UpdateFailed)
	}

	extractGroup(model, group)

	return nil
}

func (srv *GroupServiceLayer) Delete(ctx context.Context, group *domain.Group) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if group == nil {
		return errors.New(GroupNotDefined)
	}

	if group.ID == 0 {
		return errors.New(RequiredGroupID)
	}

	model, err := models.Groups(qm.Where("id = ?", group.ID)).One(c, ex)
	if err == sql.ErrNoRows {
		return errors.New(NotFound)
	}
	if err != nil {
		return errors.Wrap(err, SearchGroupFailed)
	}

	_, err = model.Delete(c, ex)
	if err != nil {
		return errors.Wrap(err, DeleteFailed)
	}

	return nil
}

func (srv *GroupServiceLayer) StatusList(ctx context.Context) ([]*domain.GroupStatus, error) {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	statuses, err := models.GroupStatuses().All(c, ex)
	if err != nil && err != sql.ErrNoRows {
		return nil, errors.Wrap(err, SearchGroupStatusesFailed)
	}

	results := make([]*domain.GroupStatus, len(statuses))

	for i, model := range statuses {
		results[i] = &domain.GroupStatus{
			ID:        model.ID,
			Name:      model.Name,
			SortOrder: model.SortOrder,
		}
	}

	return results, nil
}

func (srv *GroupServiceLayer) CreateStatus(ctx context.Context, status *domain.GroupStatus) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if status == nil {
		return errors.New(GroupStatusNotDefined)
	}

	model := &models.GroupStatus{
		ID:        status.ID,
		Name:      status.Name,
		SortOrder: status.SortOrder,
	}

	err := model.Insert(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, InsertFailed)
	}

	status.ID = model.ID

	return nil
}

func (srv *GroupServiceLayer) UpdateStatus(ctx context.Context, status *domain.GroupStatus) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if status == nil {
		return errors.New(GroupStatusNotDefined)
	}

	if status.ID == 0 {
		return errors.New(RequiredGroupStatusID)
	}

	model, err := models.GroupStatuses(qm.Where("id = ?", status.ID)).One(c, ex)
	if err == sql.ErrNoRows {
		status = nil
		return nil
	}
	if err != nil {
		return errors.New(SearchGroupStatusFailed)
	}

	model.Name = status.Name
	model.SortOrder = status.SortOrder

	_, err = model.Update(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, UpdateFailed)
	}

	return nil
}

func (srv *GroupServiceLayer) DeleteStatus(ctx context.Context, status *domain.GroupStatus) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if status == nil {
		return errors.New(GroupStatusNotDefined)
	}

	if status.ID == 0 {
		return errors.New(RequiredGroupStatusID)
	}

	model, err := models.GroupStatuses(qm.Where("id = ?", status.ID)).One(c, ex)
	if err == sql.ErrNoRows {
		status = nil
		return nil
	}
	if err != nil {
		return errors.New(SearchGroupStatusFailed)
	}

	_, err = model.Delete(c, ex)
	if err != nil {
		return errors.Wrap(err, DeleteFailed)
	}

	return nil
}

func extractGroup(model *models.Group, group *domain.Group) *domain.Group {
	if group == nil {
		group = &domain.Group{}
	}

	group.ID = model.ID
	group.Name = model.Name
	group.Status = &domain.GroupStatus{
		ID: model.StatusID,
	}

	if model.R != nil && model.R.Status != nil {
		group.Status.Name = model.R.Status.Name
	}

	return group
}

func groupModel(group *domain.Group) *models.Group {
	model := &models.Group{
		ID:   group.ID,
		Name: group.Name,
	}

	if group.Status != nil {
		model.StatusID = group.Status.ID
	}

	return model
}

// pseudo-Singleton
var groupInstance *GroupServiceLayer

var groupOnce sync.Once

func GroupService(executor layers.BoilExecutor, model *mvc.BaseModel) *GroupServiceLayer {
	groupOnce.Do(func() {
		if model == nil {
			model = &mvc.BaseModel{}
		}

		groupInstance = &GroupServiceLayer{
			BaseModel: *model,
			Executor:  executor,
		}
	})

	return groupInstance
}
