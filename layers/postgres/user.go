package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"sync"
	"time"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers"
	"bitbucket.org/zolotarev-dmitriy/accounts/models/postgres"
	"github.com/toolsparty/mvc"

	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
)

type UserServiceLayer struct {
	mvc.BaseModel

	Executor layers.BoilExecutor
}

func (UserServiceLayer) Name() (string, error) {
	return domain.UserModelName, nil
}

func (srv *UserServiceLayer) Search(ctx context.Context, filter *domain.UserFilter) ([]*domain.User, error) {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	qMods := []qm.QueryMod{
		qm.Load("Status"),
		qm.Load("Group"),
	}

	if filter != nil {
		if filter.Status > 0 {
			qMods = append(qMods, qm.Where("status_id = ?", filter.Status))
		}

		if filter.DateType != nil {
			if !filter.DTStart.IsZero() && !filter.DTEnd.IsZero() {
				qMods = append(qMods, qm.Where(
					fmt.Sprintf("%s between ? and ?", *filter.DateType),
					filter.DTStart, filter.DTEnd,
				),
				)
			} else if !filter.DTStart.IsZero() && filter.DTEnd.IsZero() {
				qMods = append(qMods, qm.Where(
					fmt.Sprintf("%s >= ?", *filter.DateType), filter.DTStart,
				),
				)
			} else if filter.DTStart.IsZero() && filter.DTEnd.IsZero() {
				qMods = append(qMods, qm.Where(
					fmt.Sprintf("%s <= ?", *filter.DateType), filter.DTEnd,
				),
				)
			}
		}

		if filter.Name != "" {
			if filter.Exactly {
				qMods = append(qMods, qm.Where("name = ?", filter.Name))
			} else {
				clause := "%" + filter.Name + "%"
				qMods = append(qMods, qm.Where("name like ?", clause))
			}
		}

		if filter.Login != "" {
			if filter.Exactly {
				qMods = append(qMods, qm.Where("login = ?", filter.Login))
			} else {
				clause := "%" + filter.Login + "%"
				qMods = append(qMods, qm.Where("login like ?", clause))
			}
		}

		if filter.Phone != "" {
			if filter.Exactly {
				qMods = append(qMods, qm.Where("phone = ?", filter.Phone))
			} else {
				clause := "%" + filter.Phone + "%"
				qMods = append(qMods, qm.Where("phone like ?", clause))
			}
		}
	}

	users, err := models.Users(qMods...).All(c, ex)
	if err != nil && err != sql.ErrNoRows {
		return nil, errors.Wrap(err, SearchUsersFailed)
	}

	results := make([]*domain.User, len(users))

	for i, model := range users {
		results[i] = extractUser(model, nil)
	}

	return results, nil
}

func (srv *UserServiceLayer) GetOne(ctx context.Context, user *domain.User) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if user == nil {
		return errors.New(RequiredUser)
	}

	if user.ID == 0 {
		return errors.New(RequiredUserID)
	}

	model, err := models.Users(
		qm.Where("id = ?", user.ID),
		qm.Load("Group"),
		qm.Load("Group.Status"),
		qm.Load("Status"),
	).One(c, ex)
	if err == sql.ErrNoRows {
		return errors.New(NotFound)
	}
	if err != nil {
		return errors.Wrap(err, SearchUserFailed)
	}

	extractUser(model, user)

	return nil
}

func (srv *UserServiceLayer) Create(ctx context.Context, user *domain.User) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if user == nil {
		return errors.New(RequiredUser)
	}

	if user.Group == nil || user.Group.ID == 0 {
		return errors.New(RequiredGroupID)
	}

	if user.Status == nil || user.Status.ID == 0 {
		return errors.New(RequiredUserStatusID)
	}

	if user.Name == "" {
		return errors.New(RequiredUserName)
	}

	if user.Login == "" {
		return errors.New(RequiredUserLogin)
	}

	if user.Password == "" {
		return errors.New(RequiredUserPassword)
	}

	model := userModel(user)
	model.DTCreated = time.Now()

	err := model.Insert(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, InsertFailed)
	}

	extractUser(model, user)

	return nil
}

func (srv *UserServiceLayer) Update(ctx context.Context, user *domain.User) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if user == nil {
		return errors.New(RequiredUser)
	}

	if user.ID == 0 {
		return errors.New(RequiredUserID)
	}

	if user.Group == nil || user.Group.ID == 0 {
		return errors.New(RequiredGroupID)
	}

	if user.Status == nil || user.Status.ID == 0 {
		return errors.New(RequiredUserStatusID)
	}

	if user.Name == "" {
		return errors.New(RequiredUserName)
	}

	if user.Login == "" {
		return errors.New(RequiredUserLogin)
	}

	if user.Password == "" {
		return errors.New(RequiredUserPassword)
	}

	model, err := models.Users(qm.Where("id = ?", user.ID)).One(c, ex)
	if err == sql.ErrNoRows {
		return errors.New(NotFound)
	}
	if err != nil {
		return errors.Wrap(err, SearchUserFailed)
	}

	model.Name = user.Name
	model.Login = user.Login
	model.Role = string(user.Role)
	model.Phone = user.Phone
	model.StatusID = user.Status.ID
	model.GroupID = user.Group.ID
	model.DTUpdated.Valid = true
	model.DTUpdated.Time = time.Now()

	if user.Password != "" {
		model.Password = user.Password
	}

	if user.DTLastLogged != nil && !user.DTLastLogged.IsZero() {
		model.DTLastLogged.Valid = true
		model.DTLastLogged.Time = *user.DTLastLogged
	}

	_, err = model.Update(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, UpdateFailed)
	}

	err = model.L.LoadGroup(c, ex, true, model, nil)
	if err != nil {
		return errors.Wrap(err, SearchGroupFailed)
	}

	err = model.L.LoadStatus(c, ex, true, model, nil)
	if err != nil {
		return errors.Wrap(err, SearchUserStatusFailed)
	}

	extractUser(model, user)

	return nil
}

func (srv *UserServiceLayer) Delete(ctx context.Context, user *domain.User) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if user == nil {
		return errors.New(RequiredUser)
	}

	if user.ID == 0 {
		return errors.New(RequiredUserID)
	}

	model, err := models.Users(qm.Where("id = ?", user.ID)).One(c, ex)
	if err == sql.ErrNoRows {
		return errors.New(NotFound)
	}
	if err != nil {
		return errors.Wrap(err, SearchUserFailed)
	}

	_, err = model.Delete(c, ex)
	if err != nil {
		return errors.Wrap(err, DeleteFailed)
	}

	return nil
}

func (srv *UserServiceLayer) StatusList(ctx context.Context) ([]*domain.UserStatus, error) {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	statuses, err := models.UserStatuses().All(c, ex)
	if err != nil && err != sql.ErrNoRows {
		return nil, errors.Wrap(err, SearchUserStatusesFailed)
	}

	results := make([]*domain.UserStatus, len(statuses))

	for i, model := range statuses {
		results[i] = &domain.UserStatus{
			ID:        model.ID,
			Name:      model.Name,
			SortOrder: model.SortOrder,
		}
	}

	return results, nil
}

func (srv *UserServiceLayer) CreateStatus(ctx context.Context, status *domain.UserStatus) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if status == nil {
		return errors.New(RequiredUserStatus)
	}

	model := &models.UserStatus{
		ID:        status.ID,
		Name:      status.Name,
		SortOrder: status.SortOrder,
	}

	err := model.Insert(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, InsertFailed)
	}

	status.ID = model.ID

	return nil
}

func (srv *UserServiceLayer) UpdateStatus(ctx context.Context, status *domain.UserStatus) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if status == nil {
		return errors.New(RequiredUserStatus)
	}

	if status.ID == 0 {
		return errors.New(RequiredUserStatusID)
	}

	model, err := models.UserStatuses(qm.Where("id = ?", status.ID)).One(c, ex)
	if err == sql.ErrNoRows {
		return errors.New(NotFound)
	}
	if err != nil {
		return errors.Wrap(err, SearchUserStatusFailed)
	}

	model.Name = status.Name
	model.SortOrder = status.SortOrder

	_, err = model.Update(c, ex, boil.Infer())
	if err != nil {
		return errors.Wrap(err, UpdateFailed)
	}

	return nil
}

func (srv *UserServiceLayer) DeleteStatus(ctx context.Context, status *domain.UserStatus) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if status == nil {
		return errors.New(RequiredUserStatus)
	}

	if status.ID == 0 {
		return errors.New(RequiredUserStatusID)
	}

	model, err := models.UserStatuses(qm.Where("id = ?", status.ID)).One(c, ex)
	if err == sql.ErrNoRows {
		return errors.New(NotFound)
	}
	if err != nil {
		return errors.Wrap(err, SearchUserStatusFailed)
	}

	_, err = model.Delete(c, ex)
	if err != nil {
		return errors.Wrap(err, DeleteFailed)
	}

	return nil
}

func extractUser(model *models.User, user *domain.User) *domain.User {
	if user == nil {
		user = &domain.User{}
	}

	user.ID = model.ID
	user.Login = model.Login
	user.Password = model.Password
	user.Name = model.Name
	user.Phone = model.Phone
	user.DTCreated = &model.DTCreated
	user.Role = domain.Role(model.Role)
	user.Status = &domain.UserStatus{
		ID: model.StatusID,
	}
	user.Group = &domain.Group{
		ID: model.GroupID,
	}

	if model.DTUpdated.Valid {
		user.DTUpdated = &model.DTUpdated.Time
	}

	if model.DTLastLogged.Valid {
		user.DTLastLogged = &model.DTLastLogged.Time
	}

	if model.Data.Valid {
		model.Data.Unmarshal(&user.Data)
	}

	if model.R != nil {
		if model.R.Status != nil {
			user.Status.Name = model.R.Status.Name
			user.Status.SortOrder = model.R.Status.SortOrder
		}

		if model.R.Group != nil {
			extractGroup(model.R.Group, user.Group)
		}
	}

	return user
}

func userModel(user *domain.User) *models.User {
	model := &models.User{
		ID:       user.ID,
		Login:    user.Login,
		Name:     user.Name,
		Password: user.Password,
		Phone:    user.Phone,
		Role:     string(user.Role),
	}

	if user.DTCreated != nil {
		model.DTCreated = *user.DTCreated
	} else {
		model.DTCreated = time.Now()
	}

	if user.DTUpdated != nil {
		model.DTUpdated.Valid = true
		model.DTUpdated.Time = *user.DTUpdated
	}

	if user.DTLastLogged != nil {
		model.DTLastLogged.Valid = true
		model.DTLastLogged.Time = *user.DTLastLogged
	}

	if user.Group != nil {
		model.GroupID = user.Group.ID
	}

	if user.Status != nil {
		model.StatusID = user.Status.ID
	}

	if user.Data != nil {
		model.Data.Valid = true
		model.Data.Marshal(user.Data)
	}

	return model
}

// pseudo-Singleton
var userInstance *UserServiceLayer

var userOnce sync.Once

func UserService(executor layers.BoilExecutor, model *mvc.BaseModel) *UserServiceLayer {
	userOnce.Do(func() {
		if model == nil {
			model = &mvc.BaseModel{}
		}

		userInstance = &UserServiceLayer{
			BaseModel: *model,
			Executor:  executor,
		}
	})

	return userInstance
}
