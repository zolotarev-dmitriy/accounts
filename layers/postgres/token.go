package postgres

import (
	"context"
	"database/sql"
	"sync"
	"time"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers"
	"bitbucket.org/zolotarev-dmitriy/accounts/models/postgres"
	"github.com/toolsparty/mvc"

	"github.com/ernsheong/grand"
	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"golang.org/x/crypto/bcrypt"
)

var TokenLifeTime = time.Hour

type TokenServiceLayer struct {
	mvc.BaseModel

	Executor layers.BoilExecutor
}

func (TokenServiceLayer) Name() (string, error) {
	return domain.TokenModelName, nil
}

func (srv *TokenServiceLayer) Get(ctx context.Context, token string) (*domain.AuthToken, error) {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	model, err := models.AuthTokens(
		qm.Where("value = ?", token),
		qm.Load("User"),
		qm.Load("User.Status"),
		qm.Load("User.Group"),
	).One(c, ex)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, SearchAuthTokenFailed)
	}

	return extractToken(model), nil
}

func (srv *TokenServiceLayer) Create(ctx context.Context, user *domain.User, status domain.AuthTokenStatus) (*domain.AuthToken, error) {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if user == nil {
		return nil, errors.New(RequiredUser)
	}

	if user.ID == 0 {
		return nil, errors.New(RequiredUserID)
	}

	str := grand.GenerateRandomString(64)
	enc, err := Encrypt([]byte(str))
	if err != nil {
		return nil, errors.Wrap(err, EncryptFailed)
	}

	now := time.Now().UTC()

	model := &models.AuthToken{
		Value:     string(enc),
		Status:    int16(status),
		DTCreated: now,
		DTExpired: now.Add(TokenLifeTime),
		UserID:    user.ID,
	}

	err = model.Insert(c, ex, boil.Infer())
	if err != nil {
		return nil, errors.Wrap(err, InsertFailed)
	}

	token := extractToken(model)
	token.User = user

	return token, nil
}

func (srv *TokenServiceLayer) Update(ctx context.Context, token string, status domain.AuthTokenStatus) (*domain.AuthToken, error) {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	model, err := models.AuthTokens(
		qm.Where("value = ?", token),
		qm.Load("User"),
		qm.Load("User.Status"),
		qm.Load("User.Group"),
	).One(c, ex)
	if err == sql.ErrNoRows {
		return nil, errors.New(TokenNotFound)
	}
	if err != nil {
		return nil, errors.Wrap(err, SearchAuthTokenFailed)
	}

	model.Status = int16(status)
	model.DTUpdated.Valid = true
	model.DTUpdated.Time = time.Now().UTC()

	_, err = model.Update(c, ex, boil.Infer())
	if err != nil {
		return nil, errors.Wrap(err, UpdateFailed)
	}

	return extractToken(model), nil
}

func (srv *TokenServiceLayer) Delete(ctx context.Context, token string) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	model, err := models.AuthTokens(
		qm.Where("value = ?", token),
	).One(c, ex)
	if err == sql.ErrNoRows {
		return errors.New(TokenNotFound)
	}
	if err != nil {
		return errors.Wrap(err, SearchAuthTokenFailed)
	}

	_, err = model.Delete(c, ex)
	if err != nil {
		return errors.Wrap(err, DeleteFailed)
	}

	return nil
}

func Encrypt(pass []byte) ([]byte, error) {
	p, err := bcrypt.GenerateFromPassword(pass, bcrypt.DefaultCost)
	if err != nil {
		return nil, errors.Wrap(err, EncryptFailed)
	}

	return p, nil
}

func extractToken(model *models.AuthToken) *domain.AuthToken {
	token := &domain.AuthToken{
		ID:        model.ID,
		Value:     model.Value,
		Status:    domain.AuthTokenStatus(model.Status),
		DTCreated: model.DTCreated,
		DTExpired: model.DTExpired,
		User: &domain.User{
			ID: model.UserID,
		},
	}

	if model.DTUpdated.Valid {
		token.DTUpdated = &model.DTUpdated.Time
	}

	if model.R != nil && model.R.User != nil {
		extractUser(model.R.User, token.User)
	}

	return token
}

// pseudo-Singleton
var tokenInstance *TokenServiceLayer

var tokenOnce sync.Once

func TokenService(executor layers.BoilExecutor, model *mvc.BaseModel) *TokenServiceLayer {
	tokenOnce.Do(func() {
		if model == nil {
			model = &mvc.BaseModel{}
		}

		tokenInstance = &TokenServiceLayer{
			BaseModel: *model,
			Executor:  executor,
		}
	})

	return tokenInstance
}
