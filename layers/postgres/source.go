package postgres

import (
	"context"
	"database/sql"
	"sync"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers"
	"bitbucket.org/zolotarev-dmitriy/accounts/models/postgres"
	"github.com/toolsparty/mvc"

	"github.com/pkg/errors"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
)

type SourceServiceLayer struct {
	mvc.BaseModel

	Executor layers.BoilExecutor
}

func (SourceServiceLayer) Name() (string, error) {
	return domain.SourceModelName, nil
}

func (srv *SourceServiceLayer) Get(ctx context.Context, sourceID int64, source string) (*domain.UserSource, error) {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	model, err := models.UserSources(
		qm.Where("source = ? and source_id = ?", source, sourceID),
	).One(c, ex)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, SearchUserSourceFailed)
	}

	return &domain.UserSource{
		ID: model.ID,
		User: &domain.User{
			ID: model.UserID,
		},
		Source:   model.Source,
		SourceID: model.SourceID,
		Policy:   domain.PolicyFromString(model.Policy),
	}, nil
}

func (srv *SourceServiceLayer) Set(ctx context.Context, source *domain.UserSource) error {
	if source == nil {
		return errors.New(RequiredSource)
	}

	if source.Source == "" || source.SourceID == 0 {
		return errors.New(RequiredSourceID)
	}

	if source.User == nil {
		return errors.New(RequiredUser)
	}

	if source.User.ID == 0 {
		return errors.New(RequiredUserID)
	}

	var err error

	c, tr := layers.GetTransactor(ctx)
	if tr == nil {
		tr, err = srv.Executor.BeginTx(c, nil)
		if err != nil {
			return errors.Wrap(err, CreatingTransactionFailed)
		}
		defer layers.ExecuteTransaction(tr, err)
	}

	model, err := models.UserSources(
		qm.Where("user_id = ?", source.User.ID),
		qm.Where("source = ?", source.Source),
		qm.Where("source_id = ?", source.SourceID),
	).One(c, tr)
	if err != nil && err != sql.ErrNoRows {
		return errors.Wrap(err, SearchUserSourceFailed)
	}

	if model == nil {
		model = &models.UserSource{
			Source:   source.Source,
			SourceID: source.SourceID,
			UserID:   source.User.ID,
			Policy:   source.Policy.String(),
		}

		err = model.Insert(c, tr, boil.Infer())
		if err != nil {
			return errors.Wrap(err, InsertFailed)
		}
	} else {
		model.Policy = source.Policy.String()

		_, err = model.Update(c, tr, boil.Whitelist("policy"))
		if err != nil {
			return errors.Wrap(err, UpdateFailed)
		}
	}

	source.ID = model.ID

	return nil
}

func (srv *SourceServiceLayer) Remove(ctx context.Context, source *domain.UserSource) error {
	c, ex := layers.GetExecutor(ctx, srv.Executor)

	if source == nil {
		return errors.New(RequiredSource)
	}

	if source.Source == "" || source.SourceID == 0 {
		return errors.New(RequiredSourceID)
	}

	if source.User == nil {
		return errors.New(RequiredUser)
	}

	if source.User.ID == 0 {
		return errors.New(RequiredUserID)
	}

	qMods := []qm.QueryMod{
		qm.Where("user_id = ?", source.User.ID),
	}

	if source.ID > 0 {
		qMods = append(qMods, qm.Where("id = ?", source.ID))
	} else {
		qMods = append(qMods, qm.Where("source = ? and source_id = ?", source.Source, source.SourceID))
	}

	model, err := models.UserSources(qMods...).One(c, ex)
	if err == sql.ErrNoRows {
		return errors.New(NotFound)
	}
	if err != nil {
		return errors.Wrap(err, SearchUserSourceFailed)
	}

	_, err = model.Delete(c, ex)
	if err != nil {
		return errors.Wrap(err, DeleteFailed)
	}

	return nil
}

// pseudo-Singleton
var sourceInstance *SourceServiceLayer

var sourceOnce sync.Once

func SourceService(executor layers.BoilExecutor, model *mvc.BaseModel) *SourceServiceLayer {
	sourceOnce.Do(func() {
		if model == nil {
			model = &mvc.BaseModel{}
		}

		sourceInstance = &SourceServiceLayer{
			BaseModel: *model,
			Executor:  executor,
		}
	})

	return sourceInstance
}
