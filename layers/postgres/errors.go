package postgres

const (
	SearchGroupsFailed        = "search groups failed"
	SearchGroupStatusesFailed = "search groups statuses failed"
	SearchGroupFailed         = "search for group failed"
	SearchGroupStatusFailed   = "search for groups status failed"
	GroupNotDefined           = "group is nil"
	GroupStatusNotDefined     = "group is nil"
	RequiredGroupID           = "required group id"
	RequiredGroupStatusID     = "required group status id"

	SearchUsersFailed        = "search user failed"
	SearchUserFailed         = "search for user failed"
	RequiredUser             = "required user"
	RequiredUserID           = "required user id"
	RequiredUserStatus       = "required status"
	RequiredUserStatusID     = "required status id"
	RequiredUserName         = "required user name"
	RequiredUserLogin        = "required user login"
	RequiredUserPassword     = "required user password"
	SearchUserStatusFailed   = "search for user status failed"
	SearchUserStatusesFailed = "search user statuses failed"

	SearchAuthTokenFailed = "search for auth token failed"
	TokenNotFound         = "token not found"
	EncryptFailed         = "bcrypt error"

	SearchUserSourceFailed = "search for user source failed"
	RequiredSource         = "required source"
	RequiredSourceID       = "required source id"

	InsertFailed              = "inserting failed"
	UpdateFailed              = "updating failed"
	DeleteFailed              = "deleting failed"
	NotFound                  = "not found"
	CreatingTransactionFailed = "creating transaction failed"
)
