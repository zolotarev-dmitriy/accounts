package domain

import (
	"context"
	"time"

	"github.com/toolsparty/mvc"
)

const UserModelName = "user"

type User struct {
	ID           int         `json:"id,omitempty"`
	Name         string      `json:"name,omitempty"`
	Login        string      `json:"login,omitempty"`
	Phone        string      `json:"phone,omitempty"`
	Password     string      `json:"password,omitempty"`
	Role         Role        `json:"role,omitempty"`
	DTCreated    *time.Time  `json:"dt_created,omitempty"`
	DTUpdated    *time.Time  `json:"dt_updated,omitempty"`
	DTLastLogged *time.Time  `json:"dt_last_logged,omitempty"`
	Data         interface{} `json:"data,omitempty"`

	Status *UserStatus `json:"status,omitempty"`
	Group  *Group      `json:"group,omitempty"`
}

type Role string

const (
	RoleOwner = "owner"
	RoleRoot  = "root"
	RoleUser  = "user"
)

type UserStatus struct {
	ID        int    `json:"id"`
	Name      string `json:"name,omitempty"`
	SortOrder int    `json:"sort_order,omitempty"`
}

type UserFilter struct {
	Name           string
	Login          string
	Phone          string
	Status         int
	DTStart, DTEnd time.Time
	DateType       *DateType

	Exactly bool
}

type DateType string

const (
	DTCreated    DateType = "dt_created"
	DTUpdated    DateType = "dt_updated"
	DTLastLogged DateType = "dt_last_logged"
)

func NewDateType(t DateType) *DateType {
	return &t
}

type UserStore interface {
	mvc.Model

	Search(ctx context.Context, filter *UserFilter) ([]*User, error)
	GetOne(ctx context.Context, user *User) error

	Create(ctx context.Context, user *User) error
	Update(ctx context.Context, user *User) error
	Delete(ctx context.Context, user *User) error

	StatusList(ctx context.Context) ([]*UserStatus, error)
	CreateStatus(ctx context.Context, status *UserStatus) error
	UpdateStatus(ctx context.Context, status *UserStatus) error
	DeleteStatus(ctx context.Context, status *UserStatus) error
}
