package domain

import (
	"context"

	"github.com/toolsparty/mvc"
)

const GroupModelName = "group"

type Group struct {
	ID     int          `json:"id"`
	Name   string       `json:"name,omitempty"`
	Status *GroupStatus `json:"status,omitempty"`
}

type GroupStatus struct {
	ID        int    `json:"id"`
	Name      string `json:"name,omitempty"`
	SortOrder int    `json:"sort_order,omitempty"`
}

type GroupStore interface {
	mvc.Model

	Search(ctx context.Context) ([]*Group, error)
	GetOne(ctx context.Context, group *Group) error

	Create(ctx context.Context, group *Group) error
	Update(ctx context.Context, group *Group) error
	Delete(ctx context.Context, group *Group) error

	StatusList(ctx context.Context) ([]*GroupStatus, error)
	CreateStatus(ctx context.Context, status *GroupStatus) error
	UpdateStatus(ctx context.Context, status *GroupStatus) error
	DeleteStatus(ctx context.Context, status *GroupStatus) error
}
