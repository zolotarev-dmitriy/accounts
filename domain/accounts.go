package domain

type AccountsClient interface {
	Auth(login, password string) (*AuthToken, error)
	AuthByToken(token string) (*AuthToken, error)
	Logout(token string) error

	CreateToken(token string, authToken AuthToken) (*AuthToken, error)

	ListGroups(token string) ([]*Group, error)
	GetGroup(token string, id int) (*Group, error)
	CreateGroup(token string, group Group) (*Group, error)
	UpdateGroup(token string, group Group) (*Group, error)
	DeleteGroup(token string, id int) error

	ListGroupStatuses(token string) ([]*GroupStatus, error)

	GetSource(token string, id int64, name string) (*UserSource, error)
	SetSource(token string, source UserSource) (*UserSource, error)
	RemoveSource(token string, id, userID int64, name string) error

	ListUsers(token string, filter *UserFilter) ([]*User, error)
	GetUser(token string, id int) (*User, error)
	CreateUser(token string, user User) (*User, error)
	UpdateUser(token string, user User) (*User, error)
	DeleteUser(token string, id int) error

	ListUserStatuses(token string) ([]*UserStatus, error)

	Token() string
}
