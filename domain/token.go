package domain

import (
	"context"
	"time"

	"github.com/toolsparty/mvc"
)

const TokenModelName = "token"

type AuthToken struct {
	ID        int64           `json:"-"`
	Value     string          `json:"value"`
	Status    AuthTokenStatus `json:"status"`
	DTCreated time.Time       `json:"dt_created"`
	DTExpired time.Time       `json:"dt_expired"`
	DTUpdated *time.Time      `json:"-"`

	User *User `json:"user"`
}

type AuthTokenStatus int16

const (
	AuthStatusNew     AuthTokenStatus = 1
	AuthStatusRefresh AuthTokenStatus = 2
	AuthStatusReset   AuthTokenStatus = 3
	AuthStatusUsed    AuthTokenStatus = 4
	AuthStatusBlocked AuthTokenStatus = 5
)

type AuthTokenStore interface {
	mvc.Model

	Get(ctx context.Context, token string) (*AuthToken, error)
	Create(ctx context.Context, user *User, status AuthTokenStatus) (*AuthToken, error)
	Update(ctx context.Context, token string, status AuthTokenStatus) (*AuthToken, error)
	Delete(ctx context.Context, token string) error
}
