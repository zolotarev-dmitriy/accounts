package domain

import (
	"context"
	"github.com/toolsparty/mvc"
	"strconv"
)

const SourceModelName = "source"

type Policy [4]int

func (p Policy) String() string {
	return strconv.Itoa(p[0]) + strconv.Itoa(p[1]) + strconv.Itoa(p[2]) + strconv.Itoa(p[3])
}

func PolicyFromString(str string) Policy {
	var policy Policy

	for i, s := range []byte(str) {
		p, _ := strconv.Atoi(string(s))
		policy[i] = p
	}

	return policy
}

type UserSource struct {
	ID       int64  `json:"id"`
	Source   string `json:"source"`
	SourceID int64  `json:"source_id"`
	Policy   Policy `json:"policy"`

	User *User `json:"user"`
}

type UserSourceStore interface {
	mvc.Model

	Get(ctx context.Context, sourceID int64, source string) (*UserSource, error)
	Set(ctx context.Context, source *UserSource) error
	Remove(ctx context.Context, source *UserSource) error
}

type Accessible interface {
	HasRead(user *User) bool
	HasWrite(user *User) bool
	HasExecute(user *User) bool
}
