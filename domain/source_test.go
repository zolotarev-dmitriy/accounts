package domain

import "testing"

func TestPolicy(t *testing.T) {
	str := "0600"
	policy := PolicyFromString(str)

	if policy.String() != str {
		t.Fatalf(
			"creating policy from string failed: expected %s, got %s",
			str, policy.String(),
		)
	}

	policy = Policy{0, 6, 0, 0}
	if policy.String() != str {
		t.Fatalf(
			"wrong string policy: expected %s, got %s",
			str, policy.String(),
		)
	}
}

func BenchmarkPolicyFromString(b *testing.B) {
	for i := 0; i < b.N; i++ {
		PolicyFromString("0644")
	}
}

func BenchmarkPolicy_String(b *testing.B) {
	policy := Policy{0, 7, 5, 5}
	for i := 0; i < b.N; i++ {
		policy.String()
	}
}
