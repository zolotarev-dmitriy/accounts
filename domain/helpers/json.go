package helpers

import (
	"encoding/json"
	"github.com/pkg/errors"
	"io"
)

func FromJSON(r io.Reader, v interface{}) error {
	dec := json.NewDecoder(r)

	err := dec.Decode(v)
	if err != nil {
		return errors.Wrap(err, "decoding from reader failed")
	}

	return nil
}

func ToJSON(w io.Writer, b interface{}) error {
	//w.Header().Set("Content-Type", "application/json")

	enc := json.NewEncoder(w)

	err := enc.Encode(b)
	if err != nil {
		return errors.Wrap(err, "encoding json failed")
	}

	return nil
}
