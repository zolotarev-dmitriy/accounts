package helpers

import (
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

func EncryptPassword(pass []byte) ([]byte, error) {
	p, err := bcrypt.GenerateFromPassword(pass, bcrypt.DefaultCost)
	if err != nil {
		return nil, errors.Wrap(err, "bcrypt error")
	}

	return p, nil
}

func ComparePassword(hash, pass string) error {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(pass))
}
