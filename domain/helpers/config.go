package helpers

import (
	"os"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

func ReadConfig(path *string) (*viper.Viper, error) {
	config := viper.New()
	config.SetConfigType("yaml")

	if *path != "" {
		configFile, err := os.Open(*path)
		if err != nil {
			return nil, errors.Wrap(err, "opening config failed")
		}

		err = config.ReadConfig(configFile)
		if err != nil {
			return nil, errors.Wrap(err, "reading config failed")
		}
	}

	return config, nil
}
