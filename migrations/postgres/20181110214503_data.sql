-- +mig Up

INSERT INTO public.group_status ("id", "name", "sort_order") VALUES
  (1, 'Enable', 1), (2, 'Disable', 2);

INSERT INTO public.user_status ("id", "name", "sort_order") VALUES
  (1, 'New', 1), (2, 'Active', 2), (3, 'Blocked', 3), (4, 'Reset', 4);

-- +mig Down

DELETE FROM public.group_status WHERE id <= 2;
DELETE FROM public.user_status WHERE id <= 4;

