-- +mig Up

CREATE TABLE public.group_status
(
  id         SERIAL            NOT NULL,
  name       CHARACTER VARYING NOT NULL,
  sort_order INTEGER           NOT NULL    DEFAULT 0,

  CONSTRAINT group_status_pkey PRIMARY KEY (id),
  CONSTRAINT group_status_uk UNIQUE (name)
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.group
(
  id        SERIAL            NOT NULL,
  name      CHARACTER VARYING NOT NULL,
  status_id INTEGER           NOT NULL,

  CONSTRAINT group_pkey PRIMARY KEY (id),
  CONSTRAINT fk_group_status_id FOREIGN KEY (status_id)
  REFERENCES public.group_status (id) MATCH SIMPLE
  ON UPDATE RESTRICT
  ON DELETE RESTRICT
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.user_status
(
  id         SERIAL            NOT NULL,
  name       CHARACTER VARYING NOT NULL,
  sort_order INTEGER           NOT NULL    DEFAULT 0,

  CONSTRAINT user_status_pkey PRIMARY KEY (id),
  CONSTRAINT user_status_uk UNIQUE (name)
)
WITH (
OIDS = FALSE
);

CREATE TYPE public.ROLE AS ENUM
('owner', 'root', 'user');

CREATE TABLE public.user
(
  id             SERIAL                      NOT NULL,
  group_id       INTEGER                     NOT NULL,
  status_id      INTEGER                     NOT NULL,
  name           CHARACTER VARYING           NOT NULL,
  login          CHARACTER VARYING           NOT NULL,
  phone          CHARACTER VARYING(16)       NOT NULL,
  password       CHARACTER VARYING(64)       NOT NULL,
  role           ROLE                        NOT NULL          DEFAULT 'user',
  dt_created     TIMESTAMP WITHOUT TIME ZONE NOT NULL          DEFAULT now(),
  dt_updated     TIMESTAMP WITHOUT TIME ZONE                   DEFAULT NULL,
  dt_last_logged TIMESTAMP WITHOUT TIME ZONE                   DEFAULT NULL,

  CONSTRAINT user_pkey PRIMARY KEY (id),
  CONSTRAINT user_login_uk UNIQUE (login),
  CONSTRAINT user_phone_uk UNIQUE (phone),
  CONSTRAINT fk_user_status_id FOREIGN KEY (status_id)
  REFERENCES public.user_status (id) MATCH SIMPLE
  ON UPDATE RESTRICT
  ON DELETE RESTRICT,
  CONSTRAINT fk_user_group_id FOREIGN KEY (group_id)
  REFERENCES public.group (id) MATCH SIMPLE
  ON UPDATE RESTRICT
  ON DELETE RESTRICT
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.auth_token
(
  id         BIGSERIAL                   NOT NULL,
  user_id    INTEGER                     NOT NULL,
  "value"    CHARACTER VARYING(64)       NOT NULL,
  status     SMALLINT                    NOT NULL,
  dt_created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT now(),
  dt_expired TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  dt_updated TIMESTAMP WITHOUT TIME ZONE,

  CONSTRAINT token_pkey PRIMARY KEY (id),
  CONSTRAINT uk_token_value UNIQUE ("value"),
  CONSTRAINT fk_user_token FOREIGN KEY (user_id)
  REFERENCES public."user" (id) MATCH SIMPLE
  ON UPDATE CASCADE
  ON DELETE CASCADE
)
WITH (
OIDS = FALSE
);

CREATE TABLE public.user_source
(
  id        BIGSERIAL            NOT NULL,
  user_id   INTEGER              NOT NULL,
  source    CHARACTER VARYING    NOT NULL,
  source_id BIGINT               NOT NULL,
  policy    CHARACTER VARYING(4) NOT NULL,

  CONSTRAINT user_source_pkey PRIMARY KEY (id),
  CONSTRAINT fk_user_source FOREIGN KEY (user_id)
  REFERENCES public."user" (id) MATCH SIMPLE
  ON UPDATE RESTRICT
  ON DELETE RESTRICT
)
WITH (
OIDS = FALSE
);

CREATE INDEX user_source_key
  ON public.user_source (source, source_id);

-- +mig Down

DROP TABLE public.user_source;
DROP TABLE public.auth_token;
DROP TABLE public.user;
DROP TABLE public.user_status;
DROP TABLE public.group;
DROP TABLE public.group_status;

DROP TYPE public.ROLE;
