-- +mig Up

ALTER TABLE public.user
  ADD data JSONB DEFAULT NULL;

-- +mig Down

ALTER TABLE public.user
  DROP COLUMN data;

