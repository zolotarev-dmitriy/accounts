// Code generated by SQLBoiler (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import "testing"

// This test suite runs each operation test in parallel.
// Example, if your database has 3 tables, the suite will run:
// table1, table2 and table3 Delete in parallel
// table1, table2 and table3 Insert in parallel, and so forth.
// It does NOT run each operation group in parallel.
// Separating the tests thusly grants avoidance of Postgres deadlocks.
func TestParent(t *testing.T) {
	t.Run("AuthTokens", testAuthTokens)
	t.Run("Groups", testGroups)
	t.Run("GroupStatuses", testGroupStatuses)
	t.Run("Users", testUsers)
	t.Run("UserSources", testUserSources)
	t.Run("UserStatuses", testUserStatuses)
}

func TestDelete(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensDelete)
	t.Run("Groups", testGroupsDelete)
	t.Run("GroupStatuses", testGroupStatusesDelete)
	t.Run("Users", testUsersDelete)
	t.Run("UserSources", testUserSourcesDelete)
	t.Run("UserStatuses", testUserStatusesDelete)
}

func TestQueryDeleteAll(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensQueryDeleteAll)
	t.Run("Groups", testGroupsQueryDeleteAll)
	t.Run("GroupStatuses", testGroupStatusesQueryDeleteAll)
	t.Run("Users", testUsersQueryDeleteAll)
	t.Run("UserSources", testUserSourcesQueryDeleteAll)
	t.Run("UserStatuses", testUserStatusesQueryDeleteAll)
}

func TestSliceDeleteAll(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensSliceDeleteAll)
	t.Run("Groups", testGroupsSliceDeleteAll)
	t.Run("GroupStatuses", testGroupStatusesSliceDeleteAll)
	t.Run("Users", testUsersSliceDeleteAll)
	t.Run("UserSources", testUserSourcesSliceDeleteAll)
	t.Run("UserStatuses", testUserStatusesSliceDeleteAll)
}

func TestExists(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensExists)
	t.Run("Groups", testGroupsExists)
	t.Run("GroupStatuses", testGroupStatusesExists)
	t.Run("Users", testUsersExists)
	t.Run("UserSources", testUserSourcesExists)
	t.Run("UserStatuses", testUserStatusesExists)
}

func TestFind(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensFind)
	t.Run("Groups", testGroupsFind)
	t.Run("GroupStatuses", testGroupStatusesFind)
	t.Run("Users", testUsersFind)
	t.Run("UserSources", testUserSourcesFind)
	t.Run("UserStatuses", testUserStatusesFind)
}

func TestBind(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensBind)
	t.Run("Groups", testGroupsBind)
	t.Run("GroupStatuses", testGroupStatusesBind)
	t.Run("Users", testUsersBind)
	t.Run("UserSources", testUserSourcesBind)
	t.Run("UserStatuses", testUserStatusesBind)
}

func TestOne(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensOne)
	t.Run("Groups", testGroupsOne)
	t.Run("GroupStatuses", testGroupStatusesOne)
	t.Run("Users", testUsersOne)
	t.Run("UserSources", testUserSourcesOne)
	t.Run("UserStatuses", testUserStatusesOne)
}

func TestAll(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensAll)
	t.Run("Groups", testGroupsAll)
	t.Run("GroupStatuses", testGroupStatusesAll)
	t.Run("Users", testUsersAll)
	t.Run("UserSources", testUserSourcesAll)
	t.Run("UserStatuses", testUserStatusesAll)
}

func TestCount(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensCount)
	t.Run("Groups", testGroupsCount)
	t.Run("GroupStatuses", testGroupStatusesCount)
	t.Run("Users", testUsersCount)
	t.Run("UserSources", testUserSourcesCount)
	t.Run("UserStatuses", testUserStatusesCount)
}

func TestHooks(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensHooks)
	t.Run("Groups", testGroupsHooks)
	t.Run("GroupStatuses", testGroupStatusesHooks)
	t.Run("Users", testUsersHooks)
	t.Run("UserSources", testUserSourcesHooks)
	t.Run("UserStatuses", testUserStatusesHooks)
}

func TestInsert(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensInsert)
	t.Run("AuthTokens", testAuthTokensInsertWhitelist)
	t.Run("Groups", testGroupsInsert)
	t.Run("Groups", testGroupsInsertWhitelist)
	t.Run("GroupStatuses", testGroupStatusesInsert)
	t.Run("GroupStatuses", testGroupStatusesInsertWhitelist)
	t.Run("Users", testUsersInsert)
	t.Run("Users", testUsersInsertWhitelist)
	t.Run("UserSources", testUserSourcesInsert)
	t.Run("UserSources", testUserSourcesInsertWhitelist)
	t.Run("UserStatuses", testUserStatusesInsert)
	t.Run("UserStatuses", testUserStatusesInsertWhitelist)
}

// TestToOne tests cannot be run in parallel
// or deadlocks can occur.
func TestToOne(t *testing.T) {
	t.Run("AuthTokenToUserUsingUser", testAuthTokenToOneUserUsingUser)
	t.Run("GroupToGroupStatusUsingStatus", testGroupToOneGroupStatusUsingStatus)
	t.Run("UserToUserStatusUsingStatus", testUserToOneUserStatusUsingStatus)
	t.Run("UserToGroupUsingGroup", testUserToOneGroupUsingGroup)
	t.Run("UserSourceToUserUsingUser", testUserSourceToOneUserUsingUser)
}

// TestOneToOne tests cannot be run in parallel
// or deadlocks can occur.
func TestOneToOne(t *testing.T) {}

// TestToMany tests cannot be run in parallel
// or deadlocks can occur.
func TestToMany(t *testing.T) {
	t.Run("GroupToUsers", testGroupToManyUsers)
	t.Run("GroupStatusToStatusGroups", testGroupStatusToManyStatusGroups)
	t.Run("UserToAuthTokens", testUserToManyAuthTokens)
	t.Run("UserToUserSources", testUserToManyUserSources)
	t.Run("UserStatusToStatusUsers", testUserStatusToManyStatusUsers)
}

// TestToOneSet tests cannot be run in parallel
// or deadlocks can occur.
func TestToOneSet(t *testing.T) {
	t.Run("AuthTokenToUserUsingAuthTokens", testAuthTokenToOneSetOpUserUsingUser)
	t.Run("GroupToGroupStatusUsingStatusGroups", testGroupToOneSetOpGroupStatusUsingStatus)
	t.Run("UserToUserStatusUsingStatusUsers", testUserToOneSetOpUserStatusUsingStatus)
	t.Run("UserToGroupUsingUsers", testUserToOneSetOpGroupUsingGroup)
	t.Run("UserSourceToUserUsingUserSources", testUserSourceToOneSetOpUserUsingUser)
}

// TestToOneRemove tests cannot be run in parallel
// or deadlocks can occur.
func TestToOneRemove(t *testing.T) {}

// TestOneToOneSet tests cannot be run in parallel
// or deadlocks can occur.
func TestOneToOneSet(t *testing.T) {}

// TestOneToOneRemove tests cannot be run in parallel
// or deadlocks can occur.
func TestOneToOneRemove(t *testing.T) {}

// TestToManyAdd tests cannot be run in parallel
// or deadlocks can occur.
func TestToManyAdd(t *testing.T) {
	t.Run("GroupToUsers", testGroupToManyAddOpUsers)
	t.Run("GroupStatusToStatusGroups", testGroupStatusToManyAddOpStatusGroups)
	t.Run("UserToAuthTokens", testUserToManyAddOpAuthTokens)
	t.Run("UserToUserSources", testUserToManyAddOpUserSources)
	t.Run("UserStatusToStatusUsers", testUserStatusToManyAddOpStatusUsers)
}

// TestToManySet tests cannot be run in parallel
// or deadlocks can occur.
func TestToManySet(t *testing.T) {}

// TestToManyRemove tests cannot be run in parallel
// or deadlocks can occur.
func TestToManyRemove(t *testing.T) {}

func TestReload(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensReload)
	t.Run("Groups", testGroupsReload)
	t.Run("GroupStatuses", testGroupStatusesReload)
	t.Run("Users", testUsersReload)
	t.Run("UserSources", testUserSourcesReload)
	t.Run("UserStatuses", testUserStatusesReload)
}

func TestReloadAll(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensReloadAll)
	t.Run("Groups", testGroupsReloadAll)
	t.Run("GroupStatuses", testGroupStatusesReloadAll)
	t.Run("Users", testUsersReloadAll)
	t.Run("UserSources", testUserSourcesReloadAll)
	t.Run("UserStatuses", testUserStatusesReloadAll)
}

func TestSelect(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensSelect)
	t.Run("Groups", testGroupsSelect)
	t.Run("GroupStatuses", testGroupStatusesSelect)
	t.Run("Users", testUsersSelect)
	t.Run("UserSources", testUserSourcesSelect)
	t.Run("UserStatuses", testUserStatusesSelect)
}

func TestUpdate(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensUpdate)
	t.Run("Groups", testGroupsUpdate)
	t.Run("GroupStatuses", testGroupStatusesUpdate)
	t.Run("Users", testUsersUpdate)
	t.Run("UserSources", testUserSourcesUpdate)
	t.Run("UserStatuses", testUserStatusesUpdate)
}

func TestSliceUpdateAll(t *testing.T) {
	t.Run("AuthTokens", testAuthTokensSliceUpdateAll)
	t.Run("Groups", testGroupsSliceUpdateAll)
	t.Run("GroupStatuses", testGroupStatusesSliceUpdateAll)
	t.Run("Users", testUsersSliceUpdateAll)
	t.Run("UserSources", testUserSourcesSliceUpdateAll)
	t.Run("UserStatuses", testUserStatusesSliceUpdateAll)
}
