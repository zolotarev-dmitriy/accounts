package main

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"

	"github.com/getsentry/raven-go"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

var (
	confPath = pflag.String("config", "./config.yaml", "Configuration file")
	db       *sql.DB
)

func main() {
	config, err := helpers.ReadConfig(confPath)
	if err != nil {
		log.Fatalf("config error: %s", err)
	}

	if sentryDSN := config.GetString("sentry.dsn"); sentryDSN != "" {
		err = raven.SetDSN(sentryDSN)
		if err != nil {
			log.Printf("sentry error: %s", err)
		}
	}

	db, err = sql.Open(config.GetString("database.driver"), config.GetString("database.prod.dsn"))
	if err != nil {
		log.Fatalf("database error: %s", err)
	}

	rootCmd := &cobra.Command{
		Use: "accounts [command]",
	}
	rootCmd.AddCommand(UserCreate, GroupCreate)

	rootCmd.Execute()
}
