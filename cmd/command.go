package main

import (
	"log"

	"bitbucket.org/zolotarev-dmitriy/accounts/domain"
	"bitbucket.org/zolotarev-dmitriy/accounts/domain/helpers"
	"bitbucket.org/zolotarev-dmitriy/accounts/layers/postgres"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"time"
)

var (
	groupName     = pflag.String("group.name", "", "group name")
	groupStatusID = pflag.Int("group.status.id", 0, "ID of group status: 1 - enabled, 2 - disabled")

	userName     = pflag.String("user.name", "", "user name")
	userPhone    = pflag.String("user.phone", "", "user phone")
	userLogin    = pflag.String("user.login", "", "user login")
	userPassword = pflag.String("user.password", "", "user password")
	userRole     = pflag.String("user.role", "", "user role: owner, root or user")
	userStatusID = pflag.Int("user.status.id", 0, "ID of user status")
	userGroupID  = pflag.Int("user.group.id", 0, "ID of user group")
)

var UserCreate = &cobra.Command{
	Use:   "user.create [--options]",
	Short: "creating user",
	Run: func(cmd *cobra.Command, args []string) {
		pass, err := helpers.EncryptPassword([]byte(*userPassword))
		if err != nil {
			log.Fatalf("encription password failed: %s", err)
		}

		now := time.Now()

		user := &domain.User{
			Name:     *userName,
			Login:    *userLogin,
			Password: string(pass),
			Role:     domain.Role(*userRole),
			Phone:    *userPhone,
			Status: &domain.UserStatus{
				ID: *userStatusID,
			},
			Group: &domain.Group{
				ID: *userGroupID,
			},
			DTCreated: &now,
		}

		service := postgres.UserService(db, nil)

		err = service.Create(nil, user)
		if err != nil {
			log.Fatalf("creating user failed: %s", err)
		}

		log.Fatalf("user created with ID=%d", user.ID)
	},
}

var GroupCreate = &cobra.Command{
	Use:   "group.create",
	Short: "creating group",
	Run: func(cmd *cobra.Command, args []string) {
		group := &domain.Group{
			Name: *groupName,
			Status: &domain.GroupStatus{
				ID: *groupStatusID,
			},
		}

		service := postgres.GroupService(db, nil)

		err := service.Create(nil, group)
		if err != nil {
			log.Fatalf("creating group failed: %s", err)
		}

		log.Printf("group created with ID=%d", group.ID)
	},
}
